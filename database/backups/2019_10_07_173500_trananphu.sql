/*
 Navicat Premium Data Transfer

 Source Server         : database
 Source Server Type    : MySQL
 Source Server Version : 100131
 Source Host           : localhost:3306
 Source Schema         : trananphu

 Target Server Type    : MySQL
 Target Server Version : 100131
 File Encoding         : 65001

 Date: 07/10/2019 17:19:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for carousels
-- ----------------------------
DROP TABLE IF EXISTS `carousels`;
CREATE TABLE `carousels`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sort` int(11) NULL DEFAULT 0,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `text_overlay` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `carousel_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of carousels
-- ----------------------------
INSERT INTO `carousels` VALUES (1, 1, 'upload/carousels/1.jpeg', '', '1', '1', '2019-09-25 09:30:12', '2019-09-29 17:06:34');
INSERT INTO `carousels` VALUES (2, 0, 'upload/carousels/2.jpeg', '', '2', '2', '2019-09-25 09:44:57', '2019-09-29 17:05:28');
INSERT INTO `carousels` VALUES (3, 0, 'upload/carousels/3.jpeg', '', 'thuy-san', '3', '2019-09-29 17:06:07', '2019-09-29 17:06:13');

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `category_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `is_recruit` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES (1, 1, 0, NULL, 'Danh Muc 1', 1, 1, NULL, 'vi', NULL, '2019-09-25 04:00:16', '2019-09-25 04:00:16', NULL, 0);
INSERT INTO `categories` VALUES (2, 2, 0, NULL, 'Giới Thiệu', 1, 0, 'Trang Giới Thiệu', 'vi', 'Trang Giới Thiệu', '2019-09-25 04:27:25', '2019-09-25 04:27:25', NULL, 0);
INSERT INTO `categories` VALUES (3, 3, 0, NULL, 'Sản Phẩm', 1, 0, 'Trang Sản Phẩm', 'vi', 'Trang Sản Phẩm', '2019-09-25 07:09:19', '2019-09-25 07:09:19', NULL, 0);
INSERT INTO `categories` VALUES (4, 4, 0, NULL, 'Thông Tin Kỹ Thuật', 1, 0, 'Trang Thông Tin Kỹ Thuật', 'vi', 'Trang Thông Tin Kỹ Thuật', '2019-09-25 07:09:49', '2019-09-30 15:46:04', NULL, 0);
INSERT INTO `categories` VALUES (5, 5, 0, NULL, 'Tin Tức', 1, 0, 'Trang Tin Tức', 'vi', 'Trang Tin Tức', '2019-09-25 07:10:06', '2019-09-25 07:10:06', NULL, 0);
INSERT INTO `categories` VALUES (6, 6, 0, NULL, 'Tuyển Dụng', 1, 0, 'Trang Tuyển Dụng', 'vi', 'Trang Tuyển Dụng', '2019-09-25 07:10:27', '2019-10-01 04:40:41', NULL, 1);
INSERT INTO `categories` VALUES (7, 7, 0, NULL, 'Liên Hệ', 1, 0, 'Trang Liên Hệ', 'vi', 'Trang Liên Hệ', '2019-09-25 07:10:44', '2019-09-25 07:10:44', NULL, 0);
INSERT INTO `categories` VALUES (8, 8, 0, NULL, 'Carousel', 1, 0, NULL, 'vi', NULL, '2019-09-25 07:33:08', '2019-09-25 07:33:08', NULL, 0);
INSERT INTO `categories` VALUES (9, 9, 0, NULL, 'Kĩ Thuật', 1, 0, NULL, 'vi', NULL, '2019-09-30 15:45:40', '2019-09-30 15:45:51', '2019-09-30 15:45:51', 0);

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `customer_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contacts
-- ----------------------------
INSERT INTO `contacts` VALUES (1, 'Thanh Tai', 'nganhbui996@gmail.com', 'Helloos', '2019-09-26 07:36:31', '2019-09-26 07:36:31');
INSERT INTO `contacts` VALUES (2, 'Thanh Tai', 'nganhbui996@gmail.com', 'Helloos', '2019-09-26 07:40:47', '2019-09-26 07:40:47');
INSERT INTO `contacts` VALUES (3, 'Thanh Tai', 'nganhbui996@gmail.com', 'Helloos', '2019-09-26 07:46:10', '2019-09-26 07:46:10');
INSERT INTO `contacts` VALUES (4, 'Thanh Tai', 'nganhbui996@gmail.com', 'Hello Ngan', '2019-09-26 07:46:52', '2019-09-26 07:46:52');
INSERT INTO `contacts` VALUES (5, 'Thanh Tai', 'cpt.macmilan2@gmail.com', 'Helloolooo', '2019-09-26 07:49:15', '2019-09-26 07:49:15');
INSERT INTO `contacts` VALUES (6, 'Tai Thanh', 'cpt.macmilan2@gmail.com', 'Hello', '2019-09-26 08:02:42', '2019-09-26 08:02:42');
INSERT INTO `contacts` VALUES (7, 'Thanh Tai', 'cpt.macmilan2@gmail.com', 'hgh', '2019-09-26 08:11:36', '2019-09-26 08:11:36');
INSERT INTO `contacts` VALUES (8, 'asdas', 'asfasf@gmail.com', 'asfasfa', '2019-09-26 08:14:52', '2019-09-26 08:14:52');

-- ----------------------------
-- Table structure for content_categories
-- ----------------------------
DROP TABLE IF EXISTS `content_categories`;
CREATE TABLE `content_categories`  (
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT 0,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`content_id`, `category_id`) USING BTREE,
  INDEX `content_categories_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `content_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `content_categories_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of content_categories
-- ----------------------------
INSERT INTO `content_categories` VALUES (1, 2, 0, 1, 0, 0, '2019-09-29 17:08:20', '2019-09-29 17:08:20', NULL);
INSERT INTO `content_categories` VALUES (2, 8, 0, 1, 0, 0, '2019-09-25 07:44:18', '2019-09-25 07:44:18', NULL);
INSERT INTO `content_categories` VALUES (3, 5, 0, 1, 0, 0, '2019-09-26 10:00:59', '2019-09-26 10:00:59', NULL);
INSERT INTO `content_categories` VALUES (4, 5, 0, 1, 0, 0, '2019-09-26 10:01:08', '2019-09-26 10:01:08', NULL);
INSERT INTO `content_categories` VALUES (5, 5, 0, 1, 0, 0, '2019-09-29 12:55:35', '2019-09-29 12:55:35', NULL);
INSERT INTO `content_categories` VALUES (6, 5, 0, 1, 0, 0, '2019-09-26 04:19:00', '2019-09-26 04:19:00', NULL);
INSERT INTO `content_categories` VALUES (7, 6, 0, 1, 0, 0, '2019-10-01 15:51:09', '2019-10-01 15:51:09', NULL);
INSERT INTO `content_categories` VALUES (8, 6, 0, 1, 0, 0, '2019-09-26 10:17:55', '2019-09-26 10:17:55', NULL);
INSERT INTO `content_categories` VALUES (9, 5, 0, 1, 0, 0, '2019-09-29 12:54:56', '2019-09-29 12:54:56', NULL);
INSERT INTO `content_categories` VALUES (10, 5, 0, 1, 0, 0, '2019-09-29 12:56:21', '2019-09-29 12:56:21', NULL);
INSERT INTO `content_categories` VALUES (12, 6, 0, 1, 0, 0, '2019-10-01 15:50:56', '2019-10-01 15:50:56', NULL);
INSERT INTO `content_categories` VALUES (13, 6, 0, 1, 0, 0, '2019-10-01 17:02:54', '2019-10-01 17:02:54', NULL);
INSERT INTO `content_categories` VALUES (14, 6, 0, 1, 0, 0, '2019-09-29 16:14:02', '2019-09-29 16:14:02', NULL);
INSERT INTO `content_categories` VALUES (15, 6, 0, 1, 0, 0, '2019-10-01 17:02:43', '2019-10-01 17:02:43', NULL);

-- ----------------------------
-- Table structure for contents
-- ----------------------------
DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `is_draft` tinyint(1) NOT NULL DEFAULT 0,
  `is_featured` tinyint(1) NOT NULL DEFAULT 0,
  `sort` int(11) NOT NULL DEFAULT 0,
  `tags` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `video` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `embed` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `contents_user_id_foreign`(`user_id`) USING BTREE,
  CONSTRAINT `contents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of contents
-- ----------------------------
INSERT INTO `contents` VALUES (1, 1, 0, NULL, 'Giới Thiệu Công Ty', 'gioi-thieu-cong-ty', 'Trân An Phú là tập đoàn thuỷ sản số 1 Việt Nam và hàng đầu trên thế giới.\r\nSản phẩm của chúng tôi hiện đang có mặt tại hơn 50 quốc gia và vùng lãnh thổ, với doanh thu trên 10,000 tỷ VNĐ mỗi năm.\r\nThông qua việc sở hữu các chuỗi giá trị khép kín và có trách nhiệm.\r\nTrân An Phú đặt mục tiêu xây dựng một hệ sinh thái hoàn chỉnh, mang lại những giá trị tốt đẹp cho tất cả các thành viên liên quan, đưa Việt Nam lên bản đồ thế giới với vị thế là nhà cung ứng tôm chất lượng hàng đầu.', '<p>Tại Tr&acirc;n An Phú, ch&uacute;ng t&ocirc;i kh&ocirc;ng ngừng kết hợp kinh nghiệm, sự s&aacute;ng tạo, v&agrave; tr&aacute;ch nhiệm trong to&agrave;n bộ chuỗi gi&aacute; trị sản xuất t&ocirc;m, từ kh&acirc;u đầu đến kh&acirc;u cuối. Sứ mệnh của ch&uacute;ng t&ocirc;i l&agrave; cung cấp cho thị trường to&agrave;n cầu những sản phẩm t&ocirc;m Việt Nam tốt nhất, sạch nhất, v&agrave; dinh dưỡng nhất; đồng thời mang đến cho người ti&ecirc;u d&ugrave;ng sự an t&acirc;m v&agrave; trải nghiệm tuyệt vời nhất tr&ecirc;n từng b&agrave;n ăn, trong từng bữa ăn.</p>\r\n\r\n<p>Điều tạo n&ecirc;n những gi&aacute; trị kh&aacute;c biệt ở Minh Ph&uacute; đ&oacute; ch&iacute;nh l&agrave; việc ch&uacute;ng t&ocirc;i sản xuất c&aacute;c sản phẩm của m&igrave;nh kh&ocirc;ng chỉ dựa tr&ecirc;n nhu cầu ti&ecirc;u d&ugrave;ng th&ocirc;ng thường, m&agrave; c&ograve;n được th&uacute;c đẩy bởi c&aacute;c gi&aacute; trị lịch sử, văn ho&aacute;, v&agrave; c&aacute;c mục ti&ecirc;u ph&aacute;t triển bền vững như: đảm bảo vệ sinh an to&agrave;n thực phẩm, bảo vệ m&ocirc;i trường, c&acirc;n bằng lợi &iacute;ch x&atilde; hội, v&agrave; quan t&acirc;m đến quyền lợi vật nu&ocirc;i.</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-09-25 04:28:38', '2019-09-29 17:08:20', NULL, 'https://www.youtube.com/watch?v=uJRv0utwSm4', 'uJRv0utwSm4');
INSERT INTO `contents` VALUES (2, 2, 0, 'upload/contents/2.jpeg', 'Carousel-1', 'carousel-1', 'Carousel-1Carousel-1Carousel-1', '<p>Carousel-1Carousel-1Carousel-1Carousel-1Carousel-1Carousel-1Carousel-1</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-09-25 07:44:17', '2019-09-25 07:44:18', NULL, '', '');
INSERT INTO `contents` VALUES (3, 3, 0, 'upload/contents/3.jpeg', 'Nuôi trồng thủy hải sản: Sử dụng thức ăn công nghiệp, giảm ô nhiễm môi trường', 'nuoi-trong-thuy-hai-san-su-dung-thuc-an-cong-nghiep-giam-o-nhiem-moi-truong', 'Thời gian qua, nhiều người nuôi tôm hùm, cá bớp… sử dụng thức ăn cá tạp làm ô nhiễm môi trường dẫn đến dịch bệnh và bị thiệt hại nặng.', '<p>Thời gian qua, nhiều người nu&ocirc;i t&ocirc;m h&ugrave;m, c&aacute; bớp&hellip; sử dụng thức ăn c&aacute; tạp l&agrave;m &ocirc; nhiễm m&ocirc;i trường dẫn đến dịch bệnh v&agrave; bị thiệt hại nặng. Ng&agrave;nh chức năng khuyến c&aacute;o, người nu&ocirc;i thủy hải sản n&ecirc;n sử dụng thức ăn c&ocirc;ng nghiệp kh&ocirc;ng chỉ giảm thiểu &ocirc; nhiễm m&ocirc;i trường m&agrave; c&ograve;n giữ trữ lượng c&aacute; tạp trong tự nhi&ecirc;n.</p>\r\n\r\n<p>Thức ăn c&aacute; tạp l&agrave;m &ocirc; nhiễm đầm, vịnh</p>\r\n\r\n<p>Theo thống k&ecirc; của Ph&ograve;ng Kinh tế TX S&ocirc;ng Cầu (tỉnh Ph&uacute; Y&ecirc;n), to&agrave;n thị x&atilde; c&oacute; tr&ecirc;n 436,5ha diện t&iacute;ch ao đ&igrave;a nu&ocirc;i trồng thủy sản, trong đ&oacute; c&aacute; m&uacute; 81,3ha, ốc hương hơn 52ha, cua 154,5ha v&agrave; gần 83.500 lồng nu&ocirc;i t&ocirc;m h&ugrave;m. Chỉ t&iacute;nh ri&ecirc;ng tại vịnh Xu&acirc;n Đ&agrave;i (TX S&ocirc;ng Cầu), hiện c&oacute; gần 40.000 lồng nu&ocirc;i t&ocirc;m h&ugrave;m, với số lồng nu&ocirc;i như vậy h&agrave;ng ng&agrave;y c&oacute; h&agrave;ng tấn thức ăn l&agrave; c&aacute; tạp tr&uacute;t xuống vịnh.</p>\r\n\r\n<p>&Ocirc;ng Trần Văn H&ugrave;ng, một người nu&ocirc;i t&ocirc;m h&ugrave;m ở x&atilde; Xu&acirc;n Phương (v&ugrave;ng nu&ocirc;i vịnh Xu&acirc;n Đ&agrave;i), cho hay: Trung b&igrave;nh mỗi lồng nu&ocirc;i 70 con t&ocirc;m h&ugrave;m, trọng lượng từ 0,6-0,8kg/con, một ng&agrave;y cho ăn 7kg mồi nhưng người nu&ocirc;i lu&ocirc;n cho ăn dư, với khoảng 10kg mồi. T&ocirc;m ăn kh&ocirc;ng hết, thải ra vịnh thối rữa, g&acirc;y &ocirc; nhiễm. Ri&ecirc;ng khu vực Xu&acirc;n Phương với gần 1.000 lồng nu&ocirc;i th&igrave; mỗi ng&agrave;y thải ra khoảng 3 tấn thức ăn thối rữa; cộng với thức ăn nu&ocirc;i c&aacute; m&uacute;, cua trong ao đ&igrave;a nu&ocirc;i ven bờ xả ra vịnh Xu&acirc;n Đ&agrave;i, g&acirc;y &ocirc; nhiễm nặng.</p>\r\n\r\n<p>C&ograve;n tại đầm C&ugrave; M&ocirc;ng (TX S&ocirc;ng Cầu), người nu&ocirc;i t&ocirc;m h&ugrave;m, c&aacute; m&uacute; cũng d&ugrave;ng c&aacute; tạp, cua ốc l&agrave;m thức ăn. Quanh đầm chưa c&oacute; b&atilde;i tập kết vỏ cua, ốc đưa l&ecirc;n bờ n&ecirc;n cứ thế đổ tr&uacute;t xuống vịnh h&igrave;nh th&agrave;nh n&uacute;i r&aacute;c. Cộng với thức ăn c&aacute; tạp thừa l&acirc;u ng&agrave;y tạo th&agrave;nh lớp b&ugrave;n h&ocirc;i thối, g&acirc;y ra dịch bệnh. &Ocirc;ng B&ugrave;i Văn Long, một người nu&ocirc;i c&aacute; m&uacute; đầm C&ugrave; M&ocirc;ng, cho hay: Trong v&ograve;ng 3 năm trở lại đ&acirc;y, năm n&agrave;o cũng xảy ra hiện tượng t&ocirc;m h&ugrave;m, c&aacute; m&uacute;, cua bệnh chết. Cũng v&igrave; vậy, gi&aacute; t&ocirc;m hạ, t&ocirc;m b&igrave;nh thường l&agrave; 1,6 triệu đồng/kg, c&ograve;n t&ocirc;m bệnh chỉ c&ograve;n 600.000 đồng/kg. C&aacute; m&uacute; bị bệnh cũng b&aacute;n đổ b&aacute;n th&aacute;o, người nu&ocirc;i thiệt hại nặng.</p>\r\n\r\n<p>B&agrave; L&ecirc; Thị Hằng Nga, Chi cục ph&oacute; Chi cục Thủy sản Ph&uacute; Y&ecirc;n, cho biết: Qua kiểm tra x&aacute;c định nguy&ecirc;n nh&acirc;n dẫn đến t&ocirc;m, c&aacute; chết l&agrave; do m&ocirc;i trường nu&ocirc;i &ocirc; nhiễm, c&aacute;c chỉ số m&ocirc;i trường vượt ngưỡng cho ph&eacute;p. Hầu hết c&aacute;c hộ nu&ocirc;i t&ocirc;m h&ugrave;m, c&aacute; m&uacute;, cua tr&ecirc;n địa b&agrave;n TX S&ocirc;ng Cầu sử dụng c&aacute; tạp l&agrave;m thức ăn cho t&ocirc;m. Trong khi đ&oacute;, c&aacute; tạp thường c&oacute; chất lượng biến động, kh&ocirc;ng đảm bảo về mặt dinh dưỡng cũng như an to&agrave;n về dịch bệnh. B&ecirc;n cạnh đ&oacute; nếu kh&ocirc;ng được bảo quản tốt sẽ dễ bị nhiễm khuẩn g&acirc;y hư thối v&agrave; biến chất. Ngo&agrave;i ra, c&aacute; tạp thừa do thủy sản nu&ocirc;i ăn kh&ocirc;ng hết rất dễ ph&acirc;n hủy, nhanh ch&oacute;ng gia tăng c&aacute;c chất độc hại, g&acirc;y &ocirc; nhiễm nguồn nước v&agrave; ph&aacute;t sinh dịch bệnh cho thủy sản nu&ocirc;i.</p>\r\n\r\n<p>Sử dụng thức ăn c&ocirc;ng nghiệp</p>\r\n\r\n<p>Thời gian qua, Trung t&acirc;m Khuyến n&ocirc;ng triển khai m&ocirc; h&igrave;nh sử dụng thức ăn c&ocirc;ng nghiệp. Theo đ&oacute;, m&ocirc; h&igrave;nh nu&ocirc;i c&aacute; chim v&acirc;y v&agrave;ng thương phẩm trong lồng, quy m&ocirc; 200m2 được triển khai tại phường Xu&acirc;n Đ&agrave;i (TX S&ocirc;ng Cầu). Sau 3 th&aacute;ng thả nu&ocirc;i, hiện c&aacute; sinh trưởng ph&aacute;t triển tốt, k&iacute;ch cỡ đạt khoảng 160-170g/con, tỉ lệ sống khoảng 80%. Thức ăn c&ocirc;ng nghiệp sử dụng c&oacute; độ đạm tr&ecirc;n 35%.</p>\r\n\r\n<p>C&ugrave;ng với đ&oacute;, m&ocirc; h&igrave;nh nu&ocirc;i c&aacute; m&uacute; thương phẩm thuộc Chương tr&igrave;nh mục ti&ecirc;u quốc gia x&acirc;y dựng n&ocirc;ng th&ocirc;n mới, quy m&ocirc; 3.400m2 được triển khai tại x&atilde; Xu&acirc;n Thịnh (TX S&ocirc;ng Cầu). Sau gần 1 th&aacute;ng thả nu&ocirc;i, c&aacute; đạt k&iacute;ch cỡ trung b&igrave;nh khoảng 12cm/con, tỉ lệ sống khoảng 95%. Thức ăn sử dụng c&oacute; độ đạm tr&ecirc;n 42%. C&ograve;n với m&ocirc; h&igrave;nh nu&ocirc;i c&aacute; r&ocirc; đồng, quy m&ocirc; 5.100m2, triển khai tại x&atilde; H&ograve;a Ph&uacute; (huyện T&acirc;y H&ograve;a), sau hơn nửa th&aacute;ng thả giống th&igrave; tỉ lệ sống khoảng 99%, k&iacute;ch cỡ đạt khoảng 5-6 cm/con.</p>\r\n\r\n<p>Theo b&agrave; Nga, nghề nu&ocirc;i thủy sản biển đang ph&aacute;t triển mạnh. Do th&oacute;i quen sử dụng c&aacute; tươi l&agrave;m thức ăn cho t&ocirc;m, c&aacute; n&ecirc;n phần lớn người nu&ocirc;i hiện nay c&ograve;n rất e d&egrave; khi sử dụng thức ăn vi&ecirc;n c&ocirc;ng nghiệp. Tuy nhi&ecirc;n, nếu so s&aacute;nh th&igrave; thức ăn vi&ecirc;n c&ocirc;ng nghiệp ngo&agrave;i việc c&oacute; được th&agrave;nh phần dinh dưỡng chuy&ecirc;n biệt ph&ugrave; hợp cho từng lo&agrave;i nu&ocirc;i th&igrave; c&ograve;n c&oacute; nhiều ưu thế vượt trội hơn so với c&aacute; tạp. Theo đ&oacute;, so với c&aacute; tạp, thức ăn vi&ecirc;n c&ocirc;ng nghiệp lu&ocirc;n được kiểm so&aacute;t chặt chẽ về mầm bệnh cũng như đảm bảo ổn định về mặt dinh dưỡng.</p>\r\n\r\n<p>Ngo&agrave;i ra với đặc t&iacute;nh l&agrave; c&oacute; độ ti&ecirc;u h&oacute;a cao, &iacute;t chất thải v&agrave; độ bền trong nước tốt, thức ăn vi&ecirc;n c&ocirc;ng nghiệp rất &iacute;t g&acirc;y &ocirc; nhiễm m&ocirc;i trường nước, qua đ&oacute; g&oacute;p phần v&agrave;o việc bảo vệ sức khỏe v&agrave; kiểm so&aacute;t tốt dịch bệnh cho thủy sản nu&ocirc;i. &ldquo;Việc sử dụng c&aacute; tạp l&agrave;m thức ăn cho thủy sản nu&ocirc;i đ&atilde; gi&aacute;n tiếp g&acirc;y n&ecirc;n sự tận diệt nguồn thủy sản đ&aacute;nh bắt tự nhi&ecirc;n. Sử dụng thức ăn vi&ecirc;n c&ocirc;ng nghiệp thay thế nguồn nguy&ecirc;n liệu biển l&agrave; một giải ph&aacute;p t&iacute;ch cực, g&oacute;p phần giảm &aacute;p lực l&ecirc;n việc khai th&aacute;c qu&aacute; mức c&aacute;c lo&agrave;i thủy sản biển khơi&rdquo;, b&agrave; Nga n&oacute;i.</p>\r\n\r\n<p>&Ocirc;ng Nguyễn Khắc T&acirc;n, Ph&oacute; Gi&aacute;m đốc Trung t&acirc;m Khuyến n&ocirc;ng cho biết: Thức ăn cho động vật thủy sản thường chiếm 35-65% tổng chi ph&iacute; một vụ nu&ocirc;i. Do vậy, để đ&aacute;p ứng nhu cầu về thức ăn cho động vật thủy sản th&igrave; việc lựa chọn v&agrave; sử dụng thức ăn l&agrave; vấn đề đ&aacute;ng quan t&acirc;m. C&aacute;c loại thức ăn ch&iacute;nh được sử dụng trong nu&ocirc;i trồng thủy sản bao gồm thức ăn c&aacute; tạp, thức ăn chế biến v&agrave; thức ăn c&ocirc;ng nghiệp. Trong đ&oacute;, việc sử dụng thức ăn c&ocirc;ng nghiệp trong hoạt động nu&ocirc;i trồng thủy sản l&agrave; xu thế tất yếu v&agrave; thiết thực, khi m&agrave; sức &eacute;p cho ng&agrave;nh khai th&aacute;c v&agrave; trữ lượng c&aacute; tạp từ tự nhi&ecirc;n đ&atilde; giảm s&uacute;t đ&aacute;ng kể.</p>\r\n\r\n<p>B&ecirc;n cạnh đ&oacute;, sử dụng thức ăn c&aacute; tạp c&ograve;n l&agrave; một trong những nguy cơ dẫn đến &ocirc; nhiễm m&ocirc;i trường v&agrave; dịch bệnh tr&ecirc;n động vật thủy sản. Hiện tại, tr&ecirc;n thị trường c&oacute; nhiều nh&atilde;n hiệu thức ăn c&ocirc;ng nghiệp được b&aacute;n để sử dụng cho động vật thủy sản kể cả nước mặn, lợ v&agrave; ngọt. Tại Ph&uacute; Y&ecirc;n, c&aacute;c sản phẩm thức ăn c&ocirc;ng nghiệp d&agrave;nh cho c&aacute;c loại thủy sản như t&ocirc;m thẻ ch&acirc;n trắng, t&ocirc;m s&uacute;, c&aacute; m&uacute;, c&aacute; chim v&acirc;y v&agrave;ng, c&aacute; nước ngọt da trơn, c&aacute; nước ngọt c&oacute; vảy tương đối đa dạng.</p>\r\n\r\n<p>MẠNH L&Ecirc; TR&Acirc;M</p>', 1, 0, 1, 1, NULL, NULL, 'vi', 0, NULL, '2019-09-26 04:06:47', '2019-09-26 10:00:59', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (4, 4, 0, 'upload/contents/4.jpeg', 'Làm giàu nhờ nuôi cá chép giòn', 'lam-giau-nho-nuoi-ca-chep-gion', 'Mô hình nuôi cá chép giòn trong lồng bè cho thu nhập cao của ông Nguyễn Văn Tung, xóm 1, xã Xuân Châu (Xuân Trường, tỉnh Nam Định) đang được nhiều người tham quan, học hỏi để thay đổi phương thức nuôi cá, áp dụng tiến bộ kỹ thuật mới nâng cao thu nhập và đời sống gia đình.', '<p>Gần 20 năm gắn b&oacute; với nghề nu&ocirc;i thủy sản, &ocirc;ng Tung lu&ocirc;n năng động, chịu kh&oacute; trong việc t&igrave;m t&ograve;i, ph&aacute;t hiện v&agrave; nu&ocirc;i thử nghiệm c&aacute;c đối tượng nu&ocirc;i mới như c&aacute; lăng chấm, c&aacute; di&ecirc;u hồng trong ao. Khoảng gần chục năm trở lại đ&acirc;y, &ocirc;ng d&ugrave;ng số tiền t&iacute;ch g&oacute;p đầu tư ph&aacute;t triển nu&ocirc;i c&aacute; lồng tr&ecirc;n s&ocirc;ng Hồng. Đến nay, &ocirc;ng Tung đ&atilde; c&oacute; 25 lồng nu&ocirc;i 3 loại c&aacute; l&agrave; ch&eacute;p gi&ograve;n, lăng chấm, di&ecirc;u hồng. Theo &ocirc;ng, đ&acirc;y l&agrave; c&aacute;c loại c&aacute; dễ nu&ocirc;i, &iacute;t bệnh, c&oacute; hiệu quả kinh tế cao, ph&ugrave; hợp với nhu cầu của thị trường. Năm 2014, trong một lần đi học hỏi kinh nghiệm m&ocirc; h&igrave;nh nu&ocirc;i c&aacute; ch&eacute;p gi&ograve;n bằng đậu tằm tại tỉnh Hải Dương, nhận thấy đ&acirc;y l&agrave; loại c&aacute; đang được thị trường ưa chuộng, gi&aacute; trị cao hơn c&aacute;c loại c&aacute; kh&aacute;c, &ocirc;ng Tung quyết t&acirc;m thực hiện m&ocirc; h&igrave;nh n&agrave;y. Ban đầu &ocirc;ng đ&atilde; thả nu&ocirc;i thử nghiệm c&aacute; ch&eacute;p gi&ograve;n tại 1 lồng bằng thức ăn đậu tằm. Ngay trong vụ đầu ti&ecirc;n, &ocirc;ng đ&atilde; thu hoạch v&agrave; xuất b&aacute;n 2 tấn c&aacute; ch&eacute;p gi&ograve;n, l&atilde;i 30 triệu đồng, gấp 1,5 lần so với nu&ocirc;i c&aacute; ch&eacute;p th&ocirc;ng thường. Từ th&agrave;nh c&ocirc;ng ban đầu, &ocirc;ng Tung mở rộng quy m&ocirc; nu&ocirc;i c&aacute; ch&eacute;p gi&ograve;n l&ecirc;n 6 lồng, trong đ&oacute; 3 lồng nu&ocirc;i ương con giống, 3 lồng nu&ocirc;i l&agrave;m gi&ograve;n c&aacute;.</p>\r\n\r\n<p>&Ocirc;ng Tung cho biết: Loại c&aacute; ch&eacute;p &ocirc;ng nu&ocirc;i l&agrave; giống c&aacute; ch&eacute;p V1, được Viện Nghi&ecirc;n cứu Nu&ocirc;i trồng Thủy sản I lai tạo giữa 3 giống c&aacute; ch&eacute;p trắng Việt Nam, c&aacute; ch&eacute;p vẩy của Hungary v&agrave; c&aacute; ch&eacute;p v&agrave;ng của Indonesia, c&oacute; tốc độ ph&aacute;t triển nhanh hơn, thịt thơm ngon hơn loại c&aacute; ch&eacute;p địa phương. C&aacute; ch&eacute;p gi&ograve;n c&oacute; nhiều ưu thế như dễ nu&ocirc;i, so với c&aacute;c loại c&aacute; kh&aacute;c &iacute;t dịch bệnh hơn v&agrave; đặc biệt sức tăng trưởng nhanh, gi&aacute; cao n&ecirc;n thu lợi nhuận kh&aacute;. Nu&ocirc;i theo kiểu lồng b&egrave; tr&ecirc;n s&ocirc;ng kết hợp thức ăn c&ocirc;ng nghiệp v&agrave; đậu tằm, sau 14 th&aacute;ng nu&ocirc;i c&aacute; đạt trọng lượng 2,5-3 kg/con. Thức ăn của c&aacute; ch&eacute;p chủ yếu l&agrave; thức ăn c&ocirc;ng nghiệp cho giai đoạn đầu. Ở 3 lồng nu&ocirc;i ương từ c&aacute; giống, &ocirc;ng Tung cho ăn thức ăn vi&ecirc;n c&ocirc;ng nghiệp cũng như c&aacute;c loại c&aacute; kh&aacute;c. Sau 8 th&aacute;ng nu&ocirc;i ương, &ocirc;ng lựa chọn những con cỡ 2,5kg trở l&ecirc;n để chuyển sang cho ăn bằng đậu tằm, gi&uacute;p tăng độ gi&ograve;n của thịt c&aacute; ch&eacute;p. &ldquo;Đậu tằm c&oacute; h&agrave;m lượng protein 31%, gồm đủ 8 loại axit amin thiết yếu, h&agrave;m lượng tinh bột 49%&hellip; l&agrave; yếu tố quyết định thay đổi chất lượng thịt của c&aacute;, tăng độ dai cơ thịt n&ecirc;n c&aacute; chắc gi&ograve;n. Nu&ocirc;i bằng đậu tằm, c&aacute; gi&ograve;n được đảm bảo sạch, an to&agrave;n vệ sinh thực phẩm&rdquo; - &ocirc;ng Tung cho hay. Thức ăn đậu tằm dễ kiếm, trong nước c&oacute; nhiều ở miền Trung, nhập khẩu th&igrave; từ Trung Quốc, Th&aacute;i Lan... Hiện &ocirc;ng đ&atilde; chuyển sang sử dụng loại đậu tằm nhập khẩu từ &Uacute;c, l&agrave; sản phẩm kh&ocirc;ng biến đổi gen nhằm n&acirc;ng cao chất lượng của c&aacute;. &Ocirc;ng Tung thường cho c&aacute; ăn v&agrave;o l&uacute;c 5 giờ chiều, khi trời m&aacute;t v&agrave; cho ăn khẩu phần đậu tằm tỷ lệ với trọng lượng c&aacute; để tr&aacute;nh t&igrave;nh trạng &ocirc; nhiễm ao nu&ocirc;i, g&acirc;y bệnh cho c&aacute;. Đậu tằm trước khi cho c&aacute; ăn được ng&acirc;m trong nước từ 12-24 giờ (nhiệt độ tr&ecirc;n 300C ng&acirc;m 12 giờ, nhiệt độ dưới 300C ng&acirc;m 24 giờ). Sau thời gian 6 th&aacute;ng nu&ocirc;i c&aacute; đạt độ dai, gi&ograve;n l&agrave; c&oacute; thể thu hoạch v&agrave; xuất b&aacute;n với gi&aacute; 90-120 ngh&igrave;n đồng/kg, c&oacute; thời điểm gi&aacute; cao tới 150 ngh&igrave;n đồng/kg, gấp 2 lần gi&aacute; c&aacute; ch&eacute;p thường. &Ocirc;ng Tung hạch to&aacute;n kinh tế, nếu điều kiện thời tiết thuận lợi, c&aacute; kh&ocirc;ng bị dịch bệnh, 1 lồng nu&ocirc;i c&aacute; ch&eacute;p th&ocirc;ng thường sau 8 th&aacute;ng sản lượng thu hoạch sẽ đạt 3-4 tấn c&aacute;, trừ chi ph&iacute; l&atilde;i 20-25 triệu đồng. Nu&ocirc;i 6 th&aacute;ng bằng đậu tằm, c&aacute; ch&eacute;p gi&ograve;n c&oacute; thể cho l&atilde;i th&ecirc;m từ 40-45 triệu đồng nữa. Do được nu&ocirc;i trong lồng c&oacute; d&ograve;ng nước chảy n&ecirc;n m&ocirc;i trường nước sạch, lu&ocirc;n được tuần ho&agrave;n, cung cấp &ocirc;-xy đầy đủ, c&aacute; vận động nhiều hơn n&ecirc;n chất lượng thịt c&aacute; dai, gi&ograve;n, kh&ocirc;ng c&oacute; m&ugrave;i tanh như c&aacute;c loại c&aacute; nu&ocirc;i th&ocirc;ng thường. Với phương thức nu&ocirc;i 3 lồng gối lứa, một năm &ocirc;ng Tung xuất b&aacute;n ra thị trường hơn 20 tấn c&aacute; ch&eacute;p gi&ograve;n cho c&aacute;c thị trường như: H&agrave; Nội, Hải Ph&ograve;ng Quảng Ninh. B&ecirc;n cạnh đ&oacute;, c&aacute;c lồng c&ograve;n lại &ocirc;ng bố tr&iacute; nu&ocirc;i c&aacute; lăng chấm, c&aacute; di&ecirc;u hồng để thu hoạch ở c&aacute;c thời điểm kh&aacute;c nhau để quay v&ograve;ng vốn mua thức ăn cho c&aacute;, ph&ugrave; hợp với điều kiện của gia đ&igrave;nh. Doanh thu từ 25 lồng c&aacute; của &ocirc;ng Tung mỗi năm đạt tr&ecirc;n 5 tỷ đồng, sau khi trừ c&aacute;c khoản chi ph&iacute; về giống, thức ăn, kh&acirc;u chăm s&oacute;c&hellip; thu l&atilde;i khoảng 1 tỷ đồng./.</p>\r\n\r\n<p>B&agrave;i v&agrave; ảnh: Ngọc &Aacute;nh</p>', 1, 0, 1, 2, NULL, NULL, 'vi', 0, NULL, '2019-09-26 04:16:53', '2019-09-26 10:01:08', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (5, 5, 0, 'upload/contents/5.jpeg', 'Khai thác tiềm năng nuôi biển theo hướng bền vững', 'khai-thac-tiem-nang-nuoi-bien-theo-huong-ben-vung', 'Từ nay đến năm 2030, Kiên Giang đầu tư phát triển khai thác tiềm năng nuôi biển theo hướng công nghiệp hiện đại, bền vững gắn với bảo vệ môi trường sinh thái, tăng thu nhập cho người dân vùng biển đảo, góp phần phát triển kinh tế biển.', '<p>Theo Sở NN&amp;PTNT Ki&ecirc;n Giang, tỉnh bố tr&iacute; nu&ocirc;i biển ở c&aacute;c huyện, th&agrave;nh phố c&oacute; biển, đảo v&agrave; quần đảo gồm Ph&uacute; Quốc, Ki&ecirc;n Hải, Ki&ecirc;n Lương, TP. H&agrave; Ti&ecirc;n, ven biển v&ugrave;ng Tứ gi&aacute;c Long Xuy&ecirc;n, U Minh Thượng ph&ugrave; hợp với điều kiện tự nhi&ecirc;n v&agrave; quy định về nu&ocirc;i biển.</p>\r\n\r\n<p>&Ocirc;ng Nguyễn Văn T&acirc;m, Gi&aacute;m đốc Sở NN&amp;PTNT Ki&ecirc;n Giang cho TTXVN biết, trước mắt, tỉnh lập đề &aacute;n ph&aacute;t triển nu&ocirc;i biển theo hướng bền vững, hiệu quả đến năm 2030; trong đ&oacute;, đ&aacute;nh gi&aacute; đ&uacute;ng, đủ c&aacute;c nguồn lực ph&aacute;t triển nu&ocirc;i biển của địa phương trong mối li&ecirc;n hệ bảo vệ nguồn lợi thủy sản, m&ocirc;i trường sinh th&aacute;i v&agrave; đảm bảo quốc ph&ograve;ng, an ninh v&ugrave;ng biển đảo. X&acirc;y dựng c&aacute;c phương &aacute;n nu&ocirc;i biển cụ thể, hiệu quả gắn với những giải ph&aacute;p đồng bộ khả thi; kết hợp ph&aacute;t triển nu&ocirc;i biển với dịch vụ thủy sản v&agrave; du lịch.</p>\r\n\r\n<p>Nu&ocirc;i biển hiện nay ở tỉnh Ki&ecirc;n Giang chủ yếu l&agrave; nu&ocirc;i c&aacute; biển v&agrave; nu&ocirc;i nhuyễn thể, nhưng c&ograve;n nhiều những kh&oacute; khăn, bất cập, hiệu quả kinh tế chưa cao.</p>\r\n\r\n<p>C&aacute; biển l&agrave; đối tượng nu&ocirc;i ch&iacute;nh theo h&igrave;nh thức nu&ocirc;i lồng b&egrave; tr&ecirc;n biển. V&ugrave;ng nu&ocirc;i tập trung quanh c&aacute;c đảo thuộc c&aacute;c huyện Ph&uacute; Quốc, Ki&ecirc;n Hải v&agrave; một số x&atilde; đảo của huyện Ki&ecirc;n Lương, TP. H&agrave; Ti&ecirc;n. Đến th&aacute;ng 9 n&agrave;y, to&agrave;n tỉnh c&oacute; 3.464 lồng nu&ocirc;i c&aacute; tr&ecirc;n biển, đạt hơn 80% kế hoạch, tăng 27,5% so với c&ugrave;ng kỳ năm 2018; dự kiến sản lượng c&aacute; thu hoạch năm 2019 hơn 3.500 tấn.</p>\r\n\r\n<p>Nu&ocirc;i nhuyễn thể được tập trung ở c&aacute;c địa phương ven biển như An Bi&ecirc;n, An Minh, H&ograve;n Đất, Ki&ecirc;n Lương v&agrave; TP. H&agrave; Ti&ecirc;n. Năm 2018, diện t&iacute;ch nu&ocirc;i nhuyễn thể gần 21.800 ha, sản lượng đạt tr&ecirc;n 66.000 tấn. Dự kiến, năm 2019, diện t&iacute;ch nu&ocirc;i nhuyễn thể to&agrave;n tỉnh hơn 22.700 ha, sản lượng ước đạt tr&ecirc;n 78.100 tấn.</p>\r\n\r\n<p>Đ&aacute;ng ch&uacute; &yacute; phải kể đến nghề nu&ocirc;i trai cấy ngọc nh&acirc;n tạo tại Ph&uacute; Quốc. Chất lượng vi&ecirc;n ngọc trai Ph&uacute; Quốc được giới chuy&ecirc;n m&ocirc;n đ&aacute;nh gi&aacute; rất cao v&agrave; lĩnh vực ng&agrave;nh nghề nu&ocirc;i trai cấy ngọc đ&atilde; mang lại n&eacute;t đặc trưng độc đ&aacute;o cho huyện đảo Ph&uacute; Quốc, g&oacute;p phần ph&aacute;t triển du lịch, thu h&uacute;t nhiều du kh&aacute;ch tham quan khi đến đảo ngọc.</p>\r\n\r\n<p>Theo l&atilde;nh đạo Sở NN&amp;PTNT Ki&ecirc;n Giang, nghề nu&ocirc;i biển của Ki&ecirc;n Giang mặc d&ugrave; c&oacute; nhiều tiềm năng nhưng thiếu ch&iacute;nh s&aacute;ch cụ thể để khuyến kh&iacute;ch đầu tư ph&aacute;t triển, hạ tầng phục vụ nu&ocirc;i biển nhiều bất cập.</p>\r\n\r\n<p>Tỉnh đang thiếu c&aacute;c nh&agrave; m&aacute;y sản xuất thức ăn c&ocirc;ng nghiệp, chế biến xuất khẩu c&aacute; thương phẩm v&agrave; cơ sở sản xuất giống c&aacute; biển phục vụ nghề nu&ocirc;i. Thị trường ti&ecirc;u thụ c&aacute;c sản phẩm nu&ocirc;i biển, li&ecirc;n kết sản xuất, ti&ecirc;u thụ c&ograve;n yếu, thị trường ti&ecirc;u thụ bấp b&ecirc;nh. Quy m&ocirc; nu&ocirc;i c&aacute; nhỏ lẻ, th&ocirc; sơ, chủ yếu nu&ocirc;i trong hộ ngư d&acirc;n, sử dụng c&aacute; tạp l&agrave;m thức ăn cho c&aacute; dẫn đến tiềm ẩn nhiều rủi ro, thiếu bền vững.</p>\r\n\r\n<p>Tỉnh sẽ triển khai đề &aacute;n ph&aacute;t triển nu&ocirc;i biển hợp l&yacute;, khoa học theo hướng hiệu quả, bền vững trong thời gian tới; bố tr&iacute;, sắp xếp lồng nu&ocirc;i v&agrave; nu&ocirc;i nhuyễn thể an to&agrave;n, ph&ugrave; hợp với quy hoạch du lịch, giao th&ocirc;ng đường biển; ph&ugrave; hợp với cảnh quan, m&ocirc;i trường sinh th&aacute;i, g&oacute;p phần tăng thu nhập, ổn định đời sống người d&acirc;n trong v&ugrave;ng v&agrave; ph&aacute;t triển kinh tế biển Ki&ecirc;n Giang.</p>\r\n\r\n<p>BT</p>', 1, 0, 1, 3, NULL, NULL, 'vi', 0, NULL, '2019-09-26 04:17:49', '2019-09-29 12:55:35', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (6, 6, 0, 'upload/contents/6.jpeg', 'Dịch bệnh đe dọa ngành tôm', 'dich-benh-de-doa-nganh-tom', 'Nhiều diện tích nuôi tôm tại hàng loạt địa phương đang bùng phát bệnh do vi bào tử trùng (EHP) và bệnh phân trắng. Nếu không có giải pháp phòng ngừa kịp thời, nguy cơ giảm sản lượng, ảnh hưởng đến kim ngạch xuất khẩu tôm.', '<p>Theo Cục Th&uacute; y (Bộ NN&amp;PTNT), ngo&agrave;i c&aacute;c bệnh tr&ecirc;n t&ocirc;m như hoại tử gan tụy cấp và bệnh đốm trắng, người nu&ocirc;i c&ograve;n canh c&aacute;nh với bệnh EHP v&agrave; bệnh ph&acirc;n trắng.</p>\r\n\r\n<p>Người nu&ocirc;i t&ocirc;m &quot;ngồi tr&ecirc;n lửa&quot;</p>\r\n\r\n<p>&Ocirc;ng Hồ Quốc Lực (chủ một trang trại nu&ocirc;i t&ocirc;m c&ocirc;ng nghệ cao ở thị x&atilde; Vĩnh Ch&acirc;u, S&oacute;c Trăng) cho biết chưa bao giờ &quot;đau tim&quot; như những ng&agrave;y qua. C&aacute;ch đ&acirc;y khoảng một th&aacute;ng, &ocirc;ng Lực thả giống gần 200ha t&ocirc;m thẻ ch&acirc;n trắng. D&ugrave; quản l&yacute; chặt chẽ từ kh&acirc;u con giống, nguồn nước, quy tr&igrave;nh nu&ocirc;i... nhưng t&ocirc;m của trang trại &ocirc;ng cũng &quot;d&iacute;nh&quot; bệnh EHP.</p>\r\n\r\n<p>&quot;Bệnh EHP tr&ecirc;n t&ocirc;m xuất hiện từ nhiều năm trước nhưng chỉ bắt đầu b&ugrave;ng ph&aacute;t từ năm 2016 v&agrave; d&acirc;y dưa cho đến nay. Vir&uacute;t sống trong nội tạng t&ocirc;m n&ecirc;n kh&oacute; diệt khiến t&ocirc;m nu&ocirc;i ho&agrave;i kh&ocirc;ng lớn, chi ph&iacute; ph&aacute;t sinh, giảm lợi nhuận&quot;, &ocirc;ng Lực lo lắng.</p>\r\n\r\n<p>&Ocirc;ng Năm Hiền, chủ trang trại nu&ocirc;i t&ocirc;m ở Bạc Li&ecirc;u, cho biết ngo&agrave;i đối ph&oacute; với bệnh EHP, &ocirc;ng c&ograve;n chi nhiều tiền để mua thuốc th&uacute; y, chế phẩm sinh học trị bệnh ph&acirc;n trắng cho t&ocirc;m. Gần 10 năm nu&ocirc;i t&ocirc;m, &ocirc;ng đ&atilde; quen hiện tượng t&ocirc;m bị bệnh ph&acirc;n trắng nhưng chưa bao giờ chứng kiến t&ocirc;m mắc bệnh n&agrave;y dữ tợn như năm nay.</p>\r\n\r\n<p>Theo &ocirc;ng Năm Hiền, một khi t&ocirc;m bị bệnh ph&acirc;n trắng, giảm kh&aacute;ng thể, ăn nhiều nhưng vẫn chậm lớn. &quot;Năng suất giảm, người nu&ocirc;i thiệt hại. Xa hơn là kh&ocirc;ng đủ nguy&ecirc;n liệu cung ứng cho c&aacute;c nh&agrave; m&aacute;y chế biến, nguy cơ ảnh hưởng đến kim ngạch xuất khẩu t&ocirc;m nếu kh&ocirc;ng c&oacute; giải ph&aacute;p ngăn chặn kịp thời&quot;, &ocirc;ng Năm Hiền ph&acirc;n trần.</p>\r\n\r\n<p>&Ocirc;ng Trần C&ocirc;ng Kh&ocirc;i - ph&oacute; vụ trưởng Vụ Nu&ocirc;i trồng thủy sản (Tổng cục Thủy sản, Bộ NN&amp;PTNT) - cho biết bệnh EHP hay bệnh ph&acirc;n trắng kh&ocirc;ng g&acirc;y chết t&ocirc;m h&agrave;ng loạt như bệnh đốm trắng, hoại tử gan tụy cấp nhưng ảnh hưởng rất lớn về kinh tế cho người nu&ocirc;i.</p>\r\n\r\n<p>&quot;Một khi t&ocirc;m bị bệnh n&agrave;y sẽ chậm lớn, thậm ch&iacute; kh&ocirc;ng lớn mặc d&ugrave; ti&ecirc;u tốn rất nhiều thức ăn. T&ocirc;m nu&ocirc;i 90 - 100 ng&agrave;y tuổi vẫn c&oacute; thể chỉ đạt k&iacute;ch cỡ bằng 4 - 5 gram/con, tức tầm 200 - 250 con/kg&quot;, &ocirc;ng Kh&ocirc;i n&oacute;i.</p>\r\n\r\n<p>Nguy cơ bệnh c&ograve;n rất cao</p>\r\n\r\n<p>T&igrave;nh h&igrave;nh nhiễm bệnh EHP tr&ecirc;n t&ocirc;m nước lợ đang c&oacute; chiều hướng gia tăng. Theo phản &aacute;nh của một số địa phương như S&oacute;c Trăng, Bạc Li&ecirc;u, Quảng Nam v&agrave; Kh&aacute;nh H&ograve;a, kết quả ph&acirc;n t&iacute;ch mẫu bệnh tr&ecirc;n t&ocirc;m giống trong th&aacute;ng 7 v&agrave; 8-2019 đ&atilde; ph&aacute;t hiện tỉ lệ nhiễm bệnh EHP kh&aacute; cao (tr&ecirc;n 11% số mẫu ph&acirc;n t&iacute;ch). Bệnh ph&acirc;n trắng cũng b&ugrave;ng ph&aacute;t mạnh tại Tr&agrave; Vinh, S&oacute;c Trăng, Bạc Li&ecirc;u v&agrave; C&agrave; Mau.</p>\r\n\r\n<p>&Ocirc;ng Tiền Ngọc Ti&ecirc;n - chi cục trưởng Chi cục Th&uacute; y v&ugrave;ng VII (Cục Th&uacute; y) - cho biết 8 th&aacute;ng đầu năm 2019, bệnh tr&ecirc;n t&ocirc;m xảy ra tại 151 x&atilde; của 48 huyện, thị x&atilde; thuộc 17 địa phương với hơn 2.000ha. Theo &ocirc;ng Ti&ecirc;n, nguy cơ dịch bệnh tr&ecirc;n t&ocirc;m trong thời gian tới rất cao do c&aacute;c loại mầm bệnh nguy hiểm c&ograve;n lưu h&agrave;nh nhiều ở c&aacute;c v&ugrave;ng nu&ocirc;i.</p>\r\n\r\n<p>Ngo&agrave;i ra, diện t&iacute;ch thiệt hại c&oacute; thể tăng mạnh trong thời gian tới do c&aacute;c điều kiện bất lợi của thời tiết như nhiệt độ, độ mặn tăng cao c&oacute; thể l&agrave;m t&ocirc;m chậm lớn (kh&ocirc;ng lột x&aacute;c), k&eacute;m ph&aacute;t triển, tạo điều kiện cho một số loại mầm bệnh ph&aacute;t triển. Do đ&oacute; cần c&oacute; giải ph&aacute;p khắc phục như quản l&yacute; m&ugrave;a vụ nu&ocirc;i, c&oacute; ao lắng trữ nước sử dụng khi cần thiết, chỉ thả giống khi đạt đủ điều kiện nu&ocirc;i, nghi&ecirc;n cứu điều chỉnh quy tr&igrave;nh nu&ocirc;i ph&ugrave; hợp v&agrave; &aacute;p dụng c&aacute;c biện ph&aacute;p tổng hợp, chống dịch bệnh.</p>\r\n\r\n<p>Theo tiến sĩ Yuri (C&ocirc;ng ty Prima Indonesia), để kiểm so&aacute;t tốt bệnh EHP, cần thực hiện nhiều giải ph&aacute;p đồng bộ như quản l&yacute; dịch bệnh, đặc biệt quản l&yacute; dịch bệnh từ đầu nguồn, tức l&agrave; từ con giống, nhất l&agrave; con giống bố mẹ. Trong đ&oacute; cần kiểm so&aacute;t chặt chẽ nguồn t&ocirc;m bố mẹ nhập khẩu từ c&aacute;c nước đang c&oacute; mầm bệnh EHP.</p>\r\n\r\n<p>&Ocirc;ng Trần Đ&igrave;nh Lu&acirc;n (tổng cục trưởng Tổng cục Thủy sản, Bộ NN&amp;PTNT): Chủ động c&aacute;c giải ph&aacute;p ứng ph&oacute;. Diễn biến thời tiết gần đ&acirc;y cho thấy biến đổi kh&iacute; hậu đang diễn ra kh&oacute; lường, nằm ngo&agrave;i v&agrave; diễn ra sớm hơn dự b&aacute;o. Thời tiết diễn biến cực đoan - một trong những nguy&ecirc;n nh&acirc;n dễ g&acirc;y ra dịch bệnh cho t&ocirc;m nu&ocirc;i, do vậy người nu&ocirc;i cần cập nhật diễn biến thời tiết kh&iacute; hậu, chủ động c&aacute;c giải ph&aacute;p ứng ph&oacute; ph&ugrave; hợp. Tổng cục Thủy sản đề nghị Cục Th&uacute; y tăng cường kiểm so&aacute;t dịch bệnh tr&ecirc;n thủy sản, nhất l&agrave; bệnh EHP, kiểm dịch trong nhập khẩu v&agrave; tăng cường x&acirc;y dựng, ph&aacute;t triển v&ugrave;ng nu&ocirc;i t&ocirc;m an to&agrave;n dịch bệnh.</p>\r\n\r\n<p>KHẮC T&Acirc;M</p>', 1, 0, 0, 4, NULL, NULL, 'vi', 0, NULL, '2019-09-26 04:19:00', '2019-09-26 04:19:00', NULL, '', '');
INSERT INTO `contents` VALUES (7, 7, 0, 'upload/contents/7.png', 'Tuyển Dụng Giám Sát Chất Lượng Sản Xuất Thủy Sản 1-2019', 'tuyẻn-dụng-giam-sat-chat-luong-san-xuat-thuy-san-1-2019', 'Phụ trách việc kiểm tra chất lượng và quy trình sản xuất thủy hải sản ở nhà máy gia công.\r\nĐảm bảo chất lượng các lô hàng đạt đủ tiêu chuẩn sử dụng cho hệ thống các nhà hàng của Nhật.\r\nBáo cáo nội dung công việc cho sếp Nhật.', '<ul>\r\n	<li>Phụ tr&aacute;ch việc kiểm tra chất lượng v&agrave; quy tr&igrave;nh sản xuất thủy hải sản ở nh&agrave; m&aacute;y gia c&ocirc;ng.</li>\r\n	<li>Đảm bảo chất lượng c&aacute;c l&ocirc; h&agrave;ng đạt đủ ti&ecirc;u chuẩn sử dụng cho hệ thống c&aacute;c nh&agrave; h&agrave;ng của Nhật.</li>\r\n	<li>B&aacute;o c&aacute;o nội dung c&ocirc;ng việc cho sếp Nhật.</li>\r\n	<li>Chi tiết c&ocirc;ng việc trao đổi trực tiếp khi phỏng vấn.</li>\r\n	<li><strong><em>L&agrave;m việc từ 8:00 ~ 17:00, thứ 2 ~ thứ 7.</em></strong></li>\r\n	<li><strong><em>Địa điểm: Thị Trấn Như Quỳnh, Văn L&acirc;m, Hưng Y&ecirc;n.</em></strong></li>\r\n</ul>\r\n\r\n<h2><strong>Kinh nghiệm/Kỹ năng chi tiết</strong></h2>\r\n\r\n<ul>\r\n	<li>Nam/nữ, tốt nghiệp đại học trở l&ecirc;n c&aacute;c chuy&ecirc;n ng&agrave;nh thủy hải sản hoặc c&ocirc;ng nghệ thực phẩm.</li>\r\n	<li>C&oacute; kinh nghiệm QC tối thiểu 3-5 năm ở c&ocirc;ng ty thủy hải sản.</li>\r\n	<li>Giao tiếp tiếng Anh tốt, ưu ti&ecirc;n biết th&ecirc;m tiếng Nhật.</li>\r\n	<li>Ứng vi&ecirc;n ở gần Hưng Y&ecirc;n, tự t&uacute;c di chuyển.</li>\r\n</ul>\r\n\r\n<h2><strong>M&ocirc; tả</strong></h2>\r\n\r\n<ul>\r\n	<li>M&atilde; việc l&agrave;m: D12998</li>\r\n	<li>Ng&agrave;nh nghề việc l&agrave;m:\r\n	<ul>\r\n		<li>Quản l&yacute; chất lượng (QA / QC)</li>\r\n		<li>Bi&ecirc;n phi&ecirc;n dịch (tiếng Nhật)</li>\r\n		<li>Thuỷ Hải Sản</li>\r\n	</ul>\r\n	</li>\r\n	<li>Cấp bậc: Nh&acirc;n vi&ecirc;n</li>\r\n	<li>Nơi l&agrave;m việc:\r\n	<ul>\r\n		<li>Hưng Y&ecirc;n , Huyện Văn L&acirc;m</li>\r\n	</ul>\r\n	</li>\r\n	<li>Tr&igrave;nh độ học vấn: Cử nh&acirc;n</li>\r\n	<li>Mức kinh nghiệm: 2-5 năm kinh nghiệm</li>\r\n	<li>Loại c&ocirc;ng việc: To&agrave;n thời gian cố định</li>\r\n	<li>Giới t&iacute;nh: Nam/Nữ</li>\r\n</ul>', 1, 0, 1, 1, NULL, NULL, 'vi', 0, NULL, '2019-09-26 10:15:09', '2019-10-01 15:51:09', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (8, 8, 0, 'upload/contents/8.jpeg', 'Nhân Viên Kinh Doanh Thuốc Thủy Sản 8-2019', 'nhan-vien-kinh-doanh-thuoc-thuy-san-8-2019', 'Khai thác, phát triển thị trường, tìm kiếm khách hàng tiềm năng (mở rộng hệ thống các cửa hàng và đại lý phân phối);\r\nĐại diện cho công ty liên hệ với các đơn vị, tổ chức để giới thiệu sản phẩm, tìm kiếm khách hàng…\r\nTrực tiếp thực hiện việc khai thác mở rộng thị trường theo sự phân công của Lãnh đạo nhằm thúc đẩy doanh số của sản phẩm.\r\nChuẩn bị nội dung chuyên môn của các tài liệu giới thiệu sản phẩm\r\nChi tiết công việc sẽ trao đổi cụ thể khi phỏng vấn.\r\nĐịa điểm công tác tại khu vực Đồng Bằng Sông Cữu Long.', '<ul>\r\n	<li>Khai th&aacute;c, ph&aacute;t triển thị trường, t&igrave;m kiếm kh&aacute;ch h&agrave;ng tiềm năng (mở rộng hệ thống c&aacute;c cửa h&agrave;ng v&agrave; đại l&yacute; ph&acirc;n phối);</li>\r\n	<li>Đại diện cho c&ocirc;ng ty li&ecirc;n hệ với c&aacute;c đơn vị, tổ chức để giới thiệu sản phẩm, t&igrave;m kiếm kh&aacute;ch h&agrave;ng&hellip;</li>\r\n	<li>Trực tiếp thực hiện việc khai th&aacute;c mở rộng thị trường theo sự ph&acirc;n c&ocirc;ng của L&atilde;nh đạo nhằm th&uacute;c đẩy doanh số của sản phẩm.</li>\r\n	<li>Chuẩn bị nội dung chuy&ecirc;n m&ocirc;n của c&aacute;c t&agrave;i liệu giới thiệu sản phẩm</li>\r\n	<li>Chi tiết c&ocirc;ng việc sẽ trao đổi cụ thể khi phỏng vấn.</li>\r\n	<li>Địa điểm c&ocirc;ng t&aacute;c tại khu vực Đồng Bằng S&ocirc;ng Cữu Long.</li>\r\n</ul>\r\n\r\n<p><strong>Y&ecirc;u cầu</strong></p>\r\n\r\n<ul>\r\n	<li>Ưu ti&ecirc;n những người c&oacute; tr&igrave;nh độ chuy&ecirc;n m&ocirc;n về ng&agrave;nh Thủy sản</li>\r\n	<li>Giới t&iacute;nh: Nam; độ tuổi từ 20-35 tuổi</li>\r\n	<li>Kinh nghiệm: y&ecirc;u cầu c&oacute; kinh nghiệm;</li>\r\n	<li>T&aacute;c phong chuy&ecirc;n nghiệp, nhiệt t&igrave;nh, chịu kh&oacute;;</li>\r\n	<li>C&oacute; khả năng giao tiếp tốt, kỹ năng đ&agrave;m ph&aacute;n, thuyết phục kh&aacute;ch h&agrave;ng;</li>\r\n	<li>Cả khả năng l&agrave;m việc độc lập v&agrave; l&agrave;m việc theo nh&oacute;m;</li>\r\n</ul>\r\n\r\n<p><strong>Quyền lợi</strong></p>\r\n\r\n<ul>\r\n	<li>Mức lương: 7 &ndash; 15tr + % doanh số</li>\r\n	<li>L&agrave;m việc trong m&ocirc;i trường năng động, chuy&ecirc;n nghiệp, hiện đại c&oacute; nhiều cơ hội thăng tiến;</li>\r\n	<li>Lương thưởng hấp dẫn, kh&ocirc;ng &aacute;p lực về doanh số</li>\r\n	<li>Hưởng đầy đủ c&aacute;c chế độ theo Luật lao động: Nghỉ lễ, tết, ph&eacute;p năm, hiếu hỷ&hellip;</li>\r\n</ul>\r\n\r\n<p><strong>Hạn nộp</strong>&nbsp;<strong>25-09-2019</strong></p>', 1, 0, 0, 2, NULL, NULL, 'vi', 0, NULL, '2019-09-26 10:17:44', '2019-09-26 10:17:55', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (9, 9, 1, 'upload/contents/9.jpeg', 'Triển khai biện pháp kiểm soát an toàn thực phẩm trong nông nghiệp', 'trien-khai-bien-phap-kiem-soat-an-toan-thuc-pham-trong-nong-nghiep', 'Theo nhận định của tổng cục thống kê, năm 2019 có nhiều khó khăn trong công tác bảo đảm an toàn vệ sinh thực phẩm (ATVSTP). Do đó, cần tăng cường công tác thanh tra, kiểm tra đột xuất và xử lý nghiêm các hành vi vi phạm ATVSTP nông, lâm, thủy sản.', '<p>Tại H&agrave; Nội, Ban chỉ đạo c&ocirc;ng t&aacute;c ATVSTP TP H&agrave; Nội cho biết, trong qu&yacute; II-2019, Ban chỉ đạo c&ocirc;ng t&aacute;c ATVSTP đ&atilde; xử phạt 1.350 cơ sở kh&ocirc;ng đảm bảo ATVSTP, phạt h&agrave;nh ch&iacute;nh gần 4,9 tỷ đồng. C&ugrave;ng thời gian tr&ecirc;n, Ban quản l&yacute; ATVSTP tại TP HCM đ&atilde; kiểm tra 2.560 cơ sở, ph&aacute;t hiện 289 cơ sở vi phạm, tiến h&agrave;nh xử phạt 243 cơ sở với tổng số tiền 3,1 tỷ đồng. Theo tổng cục thống k&ecirc;, s&aacute;u th&aacute;ng đầu năm, trong cả nước đ&atilde; xảy ra 35 vụ ngộ độc thực phẩm, c&oacute; 866 người bị ngộ độc, 5 trường hợp tử vong.</p>\r\n\r\n<p>Nguy&ecirc;n nh&acirc;n để xảy ra t&igrave;nh trạng tr&ecirc;n l&agrave; do một số tổ chức, c&aacute; nh&acirc;n sản xuất v&agrave; kinh doanh n&ocirc;ng sản, thực phẩm c&oacute; nhận thức yếu k&eacute;m n&ecirc;n cố t&igrave;nh vi phạm. Việc đ&oacute; đ&atilde; ảnh hưởng trực tiếp đến n&ocirc;ng sản &ldquo;đầu ra&rdquo; với những sản phẩm kh&ocirc;ng đảm bảo ATVSTP. Ngo&agrave;i ra, c&ograve;n do sự hạn chế trong c&ocirc;ng t&aacute;c kiểm tra, gi&aacute;m s&aacute;t, quản l&yacute; ATVSTP. Việc kiểm tra c&ograve;n chưa thực sự nghi&ecirc;m t&uacute;c n&ecirc;n chưa đ&aacute;p ứng được nhu cầu ng&agrave;y c&agrave;ng cao về ATVSTP tr&ecirc;n cả nước. Hơn nữa, quy m&ocirc; sản xuất n&ocirc;ng sản c&ograve;n nhỏ lẻ, kh&ocirc;ng đầy đủ cơ sở vật chất, trang thiết bị sơ chế n&ocirc;ng sản.</p>\r\n\r\n<p>Trước t&igrave;nh h&igrave;nh cấp thiết như vậy, Bộ N&ocirc;ng nghiệp v&agrave; ph&aacute;t triển n&ocirc;ng th&ocirc;n đ&atilde; ban h&agrave;nh quyết định &ldquo;Bảo đảm an to&agrave;n thực phẩm trong lĩnh vực n&ocirc;ng nghiệp năm 2019&rdquo;. Quyết định đ&atilde; chỉ r&otilde; phương hướng, biện ph&aacute;p để khắc phục những hạn chế yếu k&eacute;m như:</p>\r\n\r\n<p>+ C&ocirc;ng t&aacute;c chỉ đạo điều h&agrave;nh: T&iacute;ch cực chỉ đạo v&agrave; n&acirc;ng cao vai tr&ograve; của c&aacute;n bộ trong c&ocirc;ng t&aacute;c quản l&yacute;, gi&aacute;m s&aacute;t v&agrave; bảo đảm ATVSTP.</p>\r\n\r\n<p>+ Ho&agrave;n thiện cơ chế ch&iacute;nh s&aacute;ch, ph&aacute;p luật: Tập trung x&acirc;y dựng v&agrave; ban h&agrave;nh thể chế ph&aacute;p luật, quy định ph&ugrave; hợp với điều kiện của đất nước. N&acirc;ng cao hiệu lực quản l&yacute;, tạo điều kiện thuận lợi cho n&ocirc;ng nghiệp ph&aacute;t triển.</p>\r\n\r\n<p>+ Tổ chức sản xuất, ti&ecirc;u thụ n&ocirc;ng sản thực phẩm an to&agrave;n; kiểm so&aacute;t chặt chẽ ATVSTP với c&aacute;c sản phẩm nhập khẩu: Mở rộng c&aacute;c m&ocirc; h&igrave;nh sản xuất hiện đại, c&aacute;c v&ugrave;ng sản xuất tập trung quy m&ocirc; lớn c&aacute;c sản phẩm chủ lực quốc gia, sản phẩm chủ lực v&ugrave;ng ứng dụng c&ocirc;ng nghệ cao, n&ocirc;ng nghiệp hữu cơ. Tăng cường kiểm tra chất lượng sản phẩm nhập khẩu.</p>\r\n\r\n<p>+ C&ocirc;ng t&aacute;c th&ocirc;ng tin, truyền th&ocirc;ng về chất lượng, an to&agrave;n thực phẩm: Tăng cường phổ biến kiến thức, quyền lợi của người ti&ecirc;u d&ugrave;ng v&agrave; nghĩa vụ của người sản xuất. Ngo&agrave;i ra, cần tuy&ecirc;n truyền, gi&aacute;o dục về tầm quan trọng của ATVSTP đến cuộc sống v&agrave; sức khỏe của mỗi người.</p>\r\n\r\n<p>+ C&ocirc;ng t&aacute;c thanh tra, kiểm tra, gi&aacute;m s&aacute;t v&agrave; xử l&yacute; vi phạm: T&iacute;ch cực thanh tra, gi&aacute;m s&aacute;t tại c&aacute;c địa phương về vấn đề ATVSTP. Đồng thời, xử l&yacute; nghi&ecirc;m khắc v&agrave; triệt để bu&ocirc;n b&aacute;n thực phẩm giả, k&eacute;m chất lượng, kh&ocirc;ng r&otilde; nguồn gốc, bu&ocirc;n lậu thực phẩm qua bi&ecirc;n giới, việc giết mổ kh&ocirc;ng đảm bảo ATVSTP v&agrave; sử dụng bừa b&atilde;i chất cấm trong trồng trọt, chăn nu&ocirc;i v&agrave; nu&ocirc;i trồng thủy sản.</p>\r\n\r\n<p>+ Tổ chức lực lượng n&acirc;ng cao năng lực: Đ&agrave;o tạo c&aacute;n bộ quản l&yacute; chất lượng vật tư n&ocirc;ng nghiệp, ATVSTP n&ocirc;ng &ndash; l&acirc;m &ndash; thủy sản. N&acirc;ng cao chuy&ecirc;n m&ocirc;n nghiệp vụ v&agrave; kỹ năng trong tuy&ecirc;n truyền phổ biến ph&aacute;p luật ATVSTP.</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-09-29 12:54:56', '2019-09-29 12:54:56', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (10, 10, 1, 'upload/contents/10.jpeg', 'GDP 9 tháng tăng cao nhất trong gần 10 năm qua', 'gdp-9-thang-tang-cao-nhat-trong-gan-10-nam-qua', 'Ngày 28-9, tại cuộc họp báo do Tổng cục Thống kê tổ chức, ông Nguyễn Bích Lâm, Tổng cục trưởng Tổng cục Thống kê cho biết, tổng sản phẩm trong nước (GDP) 9 tháng năm 2019 ước tính tăng 6,98% so với cùng kỳ năm trước, là mức tăng cao nhất của 9 tháng trong 9 năm gần đây.', '<p>Trong mức tăng chung của to&agrave;n nền kinh tế, khu vực n&ocirc;ng, l&acirc;m nghiệp v&agrave; thủy sản tăng 2,02% (c&ugrave;ng kỳ năm 2018 tăng 3,7%), đ&oacute;ng g&oacute;p 4,8% v&agrave;o mức tăng trưởng chung; khu vực c&ocirc;ng nghiệp v&agrave; x&acirc;y dựng tăng 9,36%, đ&oacute;ng g&oacute;p 52,6%; khu vực dịch vụ tăng 6,85%, đ&oacute;ng g&oacute;p 42,6%. Động lực ch&iacute;nh của tăng trưởng kinh tế 9 th&aacute;ng năm nay l&agrave; ng&agrave;nh c&ocirc;ng nghiệp chế biến, chế tạo (tăng 11,37%) v&agrave; c&aacute;c ng&agrave;nh dịch vụ thị trường. Trong khi đ&oacute;, chỉ số gi&aacute; ti&ecirc;u d&ugrave;ng (CPI) th&aacute;ng 9 tăng 0,32% so với th&aacute;ng trước, b&igrave;nh qu&acirc;n 9 th&aacute;ng tăng 2,5% so với c&ugrave;ng kỳ năm 2018, đ&acirc;y l&agrave; mức tăng b&igrave;nh qu&acirc;n 9 th&aacute;ng thấp nhất trong 3 năm gần đ&acirc;y.<br />\r\n<br />\r\nĐ&aacute;ng lưu &yacute;, t&igrave;nh h&igrave;nh lao động, việc l&agrave;m cả nước trong 9 th&aacute;ng đầu năm cũng c&oacute; nhiều chuyển biến t&iacute;ch cực. Lực lượng lao động từ 15 tuổi trở l&ecirc;n của cả nước qu&yacute; 3-2019 ước t&iacute;nh l&agrave; 55,7 triệu người, tăng 211.700 người so với qu&yacute; trước v&agrave; tăng 263.800 người so với c&ugrave;ng kỳ năm trước; số người c&oacute; việc l&agrave;m tăng, tỷ lệ thất nghiệp v&agrave; thiếu việc l&agrave;m giảm dần. Chuyển dịch cơ cấu lao động theo hướng t&iacute;ch cực, giảm tỷ trọng lao động khu vực n&ocirc;ng, l&acirc;m nghiệp v&agrave; thủy sản, tăng tỷ trọng khu vực c&ocirc;ng nghiệp - x&acirc;y dựng v&agrave; dịch vụ, chất lượng lao động ng&agrave;y một n&acirc;ng cao, thu nhập của người lao động c&oacute; xu hướng tăng dần.<br />\r\n<br />\r\nTheo dự b&aacute;o của l&atilde;nh đạo Tổng cục Thống k&ecirc;, GDP năm 2019 dự kiến sẽ tăng 6,8%; c&ograve;n CPI tăng dưới 3%.&nbsp;<br />\r\n<br />\r\nVề du lịch, theo Tổng cục Thống k&ecirc;, t&iacute;nh chung 9 th&aacute;ng đầu năm, kh&aacute;ch quốc tế đến nước ta đạt 12,9 triệu lượt người, tăng 10,8% so với c&ugrave;ng kỳ năm trước. Th&aacute;ng 9 cũng l&agrave; th&aacute;ng thứ tư kể từ đầu năm v&agrave; l&agrave; th&aacute;ng thứ hai li&ecirc;n tiếp c&oacute; lượng kh&aacute;ch quốc tế đến Việt Nam đạt tr&ecirc;n 1,5 triệu lượt người. Kh&aacute;ch đến bằng đường bộ tăng cao nhất với 23,5%; đường h&agrave;ng kh&ocirc;ng tăng 8,3%; đường biển giảm 0,6%. Kh&aacute;ch quốc tế đến từ ch&acirc;u &Aacute; tăng 12,5%; từ ch&acirc;u &Acirc;u tăng 5,3%; kh&aacute;ch đến từ ch&acirc;u Mỹ tăng 6,8%; từ ch&acirc;u Phi tăng 10,8% so với c&ugrave;ng kỳ năm 2018... Th&ocirc;ng tin từ Diễn đ&agrave;n Kinh tế thế giới (WEF) cũng cho thấy, năng lực cạnh tranh du lịch của Việt Nam đ&atilde; cải thiện đ&aacute;ng kể, từ hạng 67/136 (năm 2017) l&ecirc;n hạng 63/140.&nbsp;</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-09-29 12:56:21', '2019-09-29 12:56:21', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (11, 11, 1, 'upload/contents/11.jpeg', 'Effectiveness of green muscardine fungus Metarhizium anisopliae and some insecticides on lesser coconut weevil Diocalandra frumenti Fabricius', 'effectiveness-of-green-muscardine-fungus-metarhizium-anisopliae-and-some-insecticides-on-lesser-coconut-weevil-diocalandra-frumenti-fabricius', 'Effectiveness of green muscardine fungus Metarhizium anisopliae and some insecticides on lesser coconut weevil Diocalandra frumenti Fabricius (Coleoptera: Curculionidae)', '<h5>Effectiveness of green muscardine fungus Metarhizium anisopliae and some insecticides on lesser coconut weevil Diocalandra frumenti Fabricius (Coleoptera: Curculionidae)Effectiveness of green muscardine fungus Metarhizium anisopliae and some insecticides on lesser coconut weevil Diocalandra frumenti Fabricius (Coleoptera: Curculionidae)</h5>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-09-29 15:46:27', '2019-09-29 16:09:49', '2019-09-29 16:09:49', NULL, NULL);
INSERT INTO `contents` VALUES (12, 11, 1, 'upload/contents/12.png', 'THÔNG BÁO TUYỂN DỤNG Ngày 18 tháng 9 năm 2019', 'thong-bao-tuyen-dung-ngay-18-thang-9-nam-2019', 'Do nhu cầu phát triển thị trường và mở rộng sản xuất kinh doanh, công ty chúng tôi cần tuyển dụng:\r\nCần tuyển 3 Giám Đốc vùng (Có kinh nghiệm vị trí tương đương) và 30 nhân viên thị trường Kiên Giang, Đồng Tháp, Miền Trung.', '<h1><strong>TH&Ocirc;NG B&Aacute;O TUYỂN DỤNG</strong></h1>\r\n\r\n<p><em>Ng&agrave;y 29 th&aacute;ng 9 năm 2019</em></p>\r\n\r\n<h1>Tuyển 3 gi&aacute;m đốc v&ugrave;ng, 30 nh&acirc;n vi&ecirc;n thị trường</h1>\r\n\r\n<p>Do nhu cầu ph&aacute;t triển thị trường v&agrave; mở rộng sản xuất kinh doanh, c&ocirc;ng ty ch&uacute;ng t&ocirc;i cần tuyển dụng:</p>\r\n\r\n<p><strong>Cần tuyển</strong>&nbsp;3 Gi&aacute;m Đốc v&ugrave;ng (C&oacute; kinh nghiệm vị tr&iacute; tương đương) v&agrave; 30 nh&acirc;n vi&ecirc;n thị trường Ki&ecirc;n Giang, Đồng Th&aacute;p, Miền Trung.</p>\r\n\r\n<p><strong>- Tr&igrave;nh độ:&nbsp;</strong>Tốt nghiệp cao đẳng, đại học.</p>\r\n\r\n<p><strong>- Chuy&ecirc;n ng&agrave;nh:</strong>&nbsp;Nu&ocirc;i trồng thủy sản hoặc chuy&ecirc;n ng&agrave;nh kinh tế.</p>\r\n\r\n<p>- Mức lương cơ bản:&nbsp;<strong>8.000.000đ - 30.000.000 đ/th&aacute;ng&nbsp;</strong>v&agrave; thưởng theo doanh số b&aacute;n h&agrave;ng&nbsp;<strong>(hoa hồng tr&ecirc;n doanh số b&aacute;n h&agrave;ng).</strong></p>\r\n\r\n<p>- C&ocirc;ng ty lu&ocirc;n tạo mọi điều kiện gi&uacute;p đỡ mỗi c&aacute; nh&acirc;n ph&aacute;t triển nghề nghiệp đồng thời x&acirc;y dựng một lực lượng từ nh&acirc;n vi&ecirc;n tới cấp quản l&yacute; l&agrave;m việc chuy&ecirc;n nghiệp.</p>\r\n\r\n<p>- Y&ecirc;u cầu đối với ứng vi&ecirc;n dự tuyển v&agrave;o c&aacute;c vị tr&iacute; gi&aacute;m đốc v&ugrave;ng v&agrave; nh&acirc;n vi&ecirc;n thị trường: Nhanh nhẹn, trung thực, chịu kh&oacute;, am hiểu về thị trường thuốc, thức ăn v&agrave; t&ocirc;m giống, ưu ti&ecirc;n người c&oacute; kinh nghiệm, c&oacute; tinh thần tr&aacute;ch nhiệm, cầu tiến...</p>\r\n\r\n<p><strong>- Hồ sơ gồm:&nbsp;</strong>Giấy kh&aacute;m sức khỏe, 1 h&igrave;nh m&agrave;u 3x4, đơn xin việc (ghi r&otilde; kinh nghiệm v&agrave; nơi đ&atilde; từng l&agrave;m việc), sơ yếu l&yacute; lịch, bản sao c&aacute;c bằng cấp chứng chỉ li&ecirc;n quan&hellip;</p>\r\n\r\n<p><strong>- Hồ sơ gửi về:&nbsp;</strong>Ph&ograve;ng tổ chức h&agrave;nh ch&iacute;nh - C&ocirc;ng ty TNHH Giống Thủy Sản Ph&aacute;t Đ&ocirc;ng Th&agrave;nh.<br />\r\nĐịa chỉ: Kh&aacute;nh Nhơn 1 - Nhơn Hải - Ninh Hải - Ninh Thuận.<br />\r\nHoặc gửi hồ sơ qua email: dongthanh279@gmail.com.<br />\r\nHoặc điện thoại trực tiếp: 0918 890 199 (Mr.Đen).</p>\r\n\r\n<p><strong>C&Ocirc;NG TY TNHH GIỐNG THỦY SẢN PH&Aacute;T Đ&Ocirc;NG TH&Agrave;NH<br />\r\nL&Agrave; NƠI HỘI TỤ V&Agrave; H&Acirc;N HOAN CH&Agrave;O Đ&Oacute;N TẤT CẢ C&Aacute;C NH&Acirc;N T&Agrave;I.</strong></p>', 1, 0, 1, 0, NULL, NULL, 'vi', 0, NULL, '2019-09-29 16:11:29', '2019-10-01 15:50:56', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (13, 13, 1, 'upload/contents/13.png', 'THÔNG BÁO TUYỂN DỤNG Ngày 20 tháng 9 năm 2019', 'thong-bao-tuyen-dung-ngay-20-thang-9-nam-2019', 'Mêkông Vina là một thương hiệu lớn và uy tín với hơn 10 năm kinh nghiệm sản xuất, kinh doanh và phân phối các mặt hàng thuốc thú y, chế phẩm sinh học, thuốc thủy sản với hệ thống phân phối rộng khắp cả nước\r\n\r\nVới đội ngũ kỹ sư thủy sản nhiều kinh nghiệp, năng động, chịu khó và ham học hỏi. Mêkông Vina tự hào khi mang đến cho khách hàng những Sản phẩm có chất lượng tốt nhất, qua đó nâng cao khả năng cạnh tranh cho khách hàng trong lĩnh vực nông nghiệp, thủy sản\r\n\r\nSứ mệnh của Mêkông Vina là trở thành một công ty Sản xuất thuốc thủy sản hàng đầu Việt Nam. Mang đến cho khách hàng những vụ nuôi thành công.', '<h2>Tuyển 10 nh&acirc;n vi&ecirc;n thị trường</h2>\r\n\r\n<p>Do nhu cầu ph&aacute;t triển v&agrave; mở rộng thị trường Cty M&ecirc;k&ocirc;ng Vina cần tuyển gấp vị tr&iacute; sau:</p>\r\n\r\n<p><strong>Nh&acirc;n vi&ecirc;n thị trường:</strong></p>\r\n\r\n<p><strong>+ Số lượng tuyển dụng:</strong>&nbsp;10 người (Cả Nam v&agrave; Nữ).</p>\r\n\r\n<p><strong>+ Mức lương:</strong></p>\r\n\r\n<p>Lương + Thưởng Doanh số cao.<br />\r\nThu nhập b&igrave;nh qu&acirc;n h&agrave;ng th&aacute;ng từ 8.000.000 đến 20 triệu/ th&aacute;ng.</p>\r\n\r\n<p><strong>+ Kinh nghiệm:</strong>&nbsp;Ưu ti&ecirc;n người c&oacute; kinh nghiệm, n&oacute;i chuyện lưu lo&aacute;t, ngoại h&igrave;nh dễ nh&igrave;n. (Nếu l&agrave; nh&acirc;n vi&ecirc;n đ&atilde; từng l&agrave;m qua Cty thuốc thủy sản sẽ được ưu ti&ecirc;n)</p>\r\n\r\n<p><strong>+ Tr&igrave;nh độ:</strong>&nbsp;Tốt nghiệp trung cấp trở l&ecirc;n chuy&ecirc;n ng&agrave;nh thủy sản, quản trị kinh doanh, kinh tế&hellip;</p>\r\n\r\n<p><strong>+ Nơi l&agrave;m việc:</strong>&nbsp;Miền T&acirc;y: C&agrave; Mau, Tr&agrave; Vinh, S&oacute;c Trăng, Ki&ecirc;n Giang.</p>\r\n\r\n<p><strong>+ H&igrave;nh thức l&agrave;m việc:</strong>&nbsp;Nh&acirc;n vi&ecirc;n ch&iacute;nh thức.</p>\r\n\r\n<p><strong>+ M&ocirc; tả c&ocirc;ng việc:</strong></p>\r\n\r\n<p>T&igrave;m kiếm, đ&agrave;m ph&aacute;n v&agrave; x&uacute;c tiến mở hệ thống Đại l&yacute; ph&acirc;n phối tr&ecirc;n địa b&agrave;n được giao;<br />\r\nHỗ trợ b&aacute;n h&agrave;ng tại Đại l&yacute; đạt doanh số theo chỉ ti&ecirc;u của C&ocirc;ng ty;<br />\r\nHướng dẫn người nu&ocirc;i sử dụng c&aacute;c sản phẩm của C&ocirc;ng ty ph&acirc;n phối v&agrave; tư vấn kỹ thuật cho người nu&ocirc;i điều trị bệnh cho t&ocirc;m, C&aacute;;<br />\r\nChi tiết c&ocirc;ng việc sẽ trao đổi cụ thể khi phỏng vấn.</p>\r\n\r\n<p><strong>+ Y&ecirc;u cầu c&ocirc;ng việc:</strong><br />\r\nC&oacute; sức khỏe tốt;<br />\r\nNh&acirc;n vi&ecirc;n Nữ c&oacute; ngoại h&igrave;nh tốt sẽ được ưu ti&ecirc;n;<br />\r\nC&oacute; kinh nghiệm trong ng&agrave;nh thủy sản, đặc biệt l&agrave; ng&agrave;nh t&ocirc;m, c&aacute;;<br />\r\nNăng động, linh hoạt, nhanh nhẹn;<br />\r\nKhả năng giao tiếp, thuyết phục;<br />\r\nKhả năng l&agrave;m việc độc lập, l&agrave;m việc nh&oacute;m.</p>\r\n\r\n<p><strong>+ Quyền lợi:</strong><br />\r\nĐược hưởng chế độ BHXH, BHYT theo quy định;<br />\r\nThưởng lễ tết, thưởng theo hiệu quả c&ocirc;ng việc, thưởng doanh số, Du lịch h&agrave;ng năm;<br />\r\nĐược đ&agrave;o tạo thường xuy&ecirc;n, cơ hội thăng tiến;<br />\r\nM&ocirc;i trường l&agrave;m việc chuy&ecirc;n nghiệp.</p>\r\n\r\n<p><strong>+ Th&ocirc;ng tin li&ecirc;n hệ:</strong>&nbsp;C&aacute;c ứng vi&ecirc;n c&oacute; mong muốn một c&ocirc;ng việc thăng tiến, ổn định l&acirc;u d&agrave;i, xin vui l&ograve;ng gửi hồ sơ ứng tuyển qua:<br />\r\nEmail:&nbsp;<a href=\"mailto:mekongvina09@gmail.com\">mekongvina09@gmail.com</a><br />\r\nHoặc gửi trực tiếp về&nbsp;<strong>Ph&ograve;ng Nh&acirc;n sự - Nh&agrave; m&aacute;y Cty M&ecirc;k&ocirc;ng Vina</strong>,&nbsp;<strong>số 199B, đường Trường Vĩnh Nguyễn, Khu vực Thạnh Mỹ, Phường Thường Thạnh, Quận C&aacute;i Răng, TP Cần Thơ.</strong><br />\r\nĐT li&ecirc;n hệ:&nbsp;<strong>Ms H&agrave;&nbsp;</strong><strong>0379.331.334 hoặc 0901.211.470.</strong></p>\r\n\r\n<p><strong>+ Thời hạn nộp hồ sơ:</strong>&nbsp;<strong><em>Từ nay đến hết 30/10/2019</em></strong></p>', 1, 0, 1, 0, NULL, NULL, 'vi', 0, NULL, '2019-09-29 16:12:30', '2019-10-01 15:51:03', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (14, 14, 1, 'upload/contents/14.jpeg', 'Thông báo tuyển dụng Ngày 23 tháng 9 năm 2019', 'thong-bao-tuyen-dung-ngay-23-thang-9-nam-2019', 'Tuyển 10 tư vấn kỹ thuật (tôm, cá)\r\nCăn cứ theo nhu cầu sản xuất phát triển kinh doanh của Công ty; Công ty có nhu cầu tuyển dụng như sau:', '<h1>Tuyển 10 tư vấn kỹ thuật (t&ocirc;m, c&aacute;)</h1>\r\n\r\n<p>Căn cứ theo nhu cầu sản xuất ph&aacute;t triển kinh doanh của C&ocirc;ng ty; C&ocirc;ng ty c&oacute; nhu cầu tuyển dụng như sau:</p>\r\n\r\n<p>Vị tr&iacute;: Tư vấn Kỹ thuật (T&ocirc;m/C&aacute;)</p>\r\n\r\n<p>Số lượng: 10</p>\r\n\r\n<p>Tr&igrave;nh độ: CĐ/ĐH</p>\r\n\r\n<p>Thời gian l&agrave;m việc: HC/03 Ca</p>\r\n\r\n<p><strong>Nơi l&agrave;m việc:&nbsp;</strong>Ki&ecirc;n Giang, Quảng Ninh, Thanh H&oacute;a, Th&aacute;i B&igrave;nh, Long An</p>\r\n\r\n<p><strong>Chế độ ph&uacute;c lợi:</strong><br />\r\n<br />\r\n&bull; Hỗ trợ 01 suất cơm giữa ca; sữa tươi<br />\r\n&bull; Lương th&aacute;ng 13, hiệu suất c&ocirc;ng việc<br />\r\n&bull; Thưởng lễ; tết<br />\r\n&bull; Du lịch h&agrave;ng năm<br />\r\n&bull; Tham gia BHXH/BHYT/BHTT/Tai nạn 24/24<br />\r\n<br />\r\n<strong>Hồ sơ ứng tuyển:</strong>&nbsp;01 bộ hồ sơ xin việc (bản photo), bao gồm:<br />\r\n<br />\r\n&bull; Sơ yếu l&yacute; lịch<br />\r\n&bull; CMND (2 bản),<br />\r\n&bull; H&igrave;nh 3x4 (2 tấm)<br />\r\n&bull; Giấy kh&aacute;m sức khỏe<br />\r\n&bull; Bằng cấp<br />\r\n&bull; Hộ khẩu (hoặc giấy tạm tr&uacute;)<br />\r\n<br />\r\n<strong>Th&ocirc;ng tin li&ecirc;n hệ:</strong><br />\r\nPh&ograve;ng Nh&acirc;n sự: (028) 3526 2313; (Ext: 192 - gặp Ms.T&acirc;m 098 398 2904);<br />\r\nEmail: tuyendung@skretting.com<br />\r\n<br />\r\nNg&agrave;y 26 th&aacute;ng 09 năm 2019<br />\r\nPH&Ograve;NG NH&Acirc;N SỰ</p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-09-29 16:14:02', '2019-09-29 16:14:02', NULL, NULL, NULL);
INSERT INTO `contents` VALUES (15, 15, 1, 'upload/contents/15.png', 'THÔNG BÁO TUYỂN DỤNG Ngày 27 tháng 9 năm 2019', 'thong-bao-tuyen-dung-ngay-27-thang-9-nam-2019', 'Công ty CP XNK hóa chất và thiết bị Kim Ngưu\r\nVị trí công việc: Nhân viên kinh doanh thuốc thú y thủy sản làm việc tại khu vực Bạc Liêu và Kiên Giang\r\nSố lượng: 02 người', '<h1>Tuyển 2 nh&acirc;n vi&ecirc;n kinh doanh</h1>\r\n\r\n<p>Vị tr&iacute; c&ocirc;ng việc: Nh&acirc;n vi&ecirc;n kinh doanh thuốc th&uacute; y thủy sản l&agrave;m việc tại khu vực Bạc Li&ecirc;u v&agrave; Ki&ecirc;n Giang</p>\r\n\r\n<p>Số lượng: 02 người</p>\r\n\r\n<p><strong>M&ocirc; tả c&ocirc;ng việc:</strong></p>\r\n\r\n<p>Chăm s&oacute;c kh&aacute;ch h&agrave;ng , t&igrave;m kiếm kh&aacute;ch h&agrave;ng v&agrave; b&aacute;n h&agrave;ng theo lịch tr&igrave;nh của c&ocirc;ng ty.<br />\r\nTrực tiếp thực hiện, đốc th&uacute;c hợp đồng Thực hiện mục ti&ecirc;u doanh số theo th&aacute;ng, qu&yacute;, năm.<br />\r\nTheo d&otilde;i c&ocirc;ng nợ, b&aacute;o c&aacute;o c&ocirc;ng việc cho cấp tr&ecirc;n trực tiếp quản l&yacute;</p>\r\n\r\n<p><strong>Y&ecirc;u cầu:</strong></p>\r\n\r\n<p>Tr&igrave;nh độ Cao Đẳng trở l&ecirc;n, chuy&ecirc;n ng&agrave;nh thủy sản<br />\r\nC&oacute; kinh nghiệm 1-2 năm.</p>\r\n\r\n<p><strong>Quyền lợi, chế độ:</strong></p>\r\n\r\n<p>Lương cơ bản 7 triệu + c&ocirc;ng t&aacute;c ph&iacute; + thưởng theo t&igrave;nh h&igrave;nh kinh doanh của c&ocirc;ng ty.</p>\r\n\r\n<p><strong>Li&ecirc;n hệ:</strong>&nbsp;C&ocirc;ng ty Cổ phần Xuất nhập khẩu H&oacute;a chất v&agrave; Thiết bị Kim Ngưu</p>\r\n\r\n<p>Ms Linh:&nbsp;<strong>0943 889 689</strong></p>\r\n\r\n<p>Ứng vi&ecirc;n c&oacute; thể nộp trực tiếp hồ sơ / CV tại VP c&ocirc;ng ty địa chỉ: Số I7/10 Đường số 9 (khu d&acirc;n cư 586) L&acirc;m Văn Phận &ndash; Ph&uacute; Thứ - C&aacute;i Răng &ndash; Cần Thơ.</p>\r\n\r\n<p>Hoặc gửi hồ sơ/ CV v&agrave; địa chỉ email:&nbsp;<strong>tuyendung@hoachat.com.vn</strong></p>', 1, 0, 0, 0, NULL, NULL, 'vi', 0, NULL, '2019-09-29 16:15:38', '2019-09-29 16:15:38', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_categories
-- ----------------------------
DROP TABLE IF EXISTS `menu_categories`;
CREATE TABLE `menu_categories`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `category_id`) USING BTREE,
  INDEX `menu_categories_category_id_foreign`(`category_id`) USING BTREE,
  CONSTRAINT `menu_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_categories_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_categories
-- ----------------------------
INSERT INTO `menu_categories` VALUES (1, 5, '2019-09-26 04:08:25', '2019-09-26 04:08:25', NULL);
INSERT INTO `menu_categories` VALUES (8, 4, '2019-09-25 07:27:30', '2019-09-25 07:27:30', NULL);
INSERT INTO `menu_categories` VALUES (9, 5, '2019-09-25 07:28:10', '2019-09-25 07:28:10', NULL);
INSERT INTO `menu_categories` VALUES (10, 6, '2019-09-25 07:29:16', '2019-09-25 07:29:16', NULL);

-- ----------------------------
-- Table structure for menu_contents
-- ----------------------------
DROP TABLE IF EXISTS `menu_contents`;
CREATE TABLE `menu_contents`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `content_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `content_id`) USING BTREE,
  INDEX `menu_contents_content_id_foreign`(`content_id`) USING BTREE,
  CONSTRAINT `menu_contents_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `contents` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_contents_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_contents
-- ----------------------------
INSERT INTO `menu_contents` VALUES (2, 1, '2019-09-25 07:11:10', '2019-09-25 07:11:10', NULL);

-- ----------------------------
-- Table structure for menu_products
-- ----------------------------
DROP TABLE IF EXISTS `menu_products`;
CREATE TABLE `menu_products`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `product_id`) USING BTREE,
  INDEX `menu_products_product_id_foreign`(`product_id`) USING BTREE,
  CONSTRAINT `menu_products_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for menu_types
-- ----------------------------
DROP TABLE IF EXISTS `menu_types`;
CREATE TABLE `menu_types`  (
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`menu_id`, `type_id`) USING BTREE,
  INDEX `menu_types_type_id_foreign`(`type_id`) USING BTREE,
  CONSTRAINT `menu_types_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `menu_types_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menu_types
-- ----------------------------
INSERT INTO `menu_types` VALUES (3, 1, '2019-09-25 07:26:03', '2019-09-25 07:26:03', NULL);
INSERT INTO `menu_types` VALUES (5, 2, '2019-09-25 07:24:33', '2019-09-25 07:24:33', NULL);
INSERT INTO `menu_types` VALUES (6, 3, '2019-09-25 07:25:24', '2019-09-25 07:25:24', NULL);
INSERT INTO `menu_types` VALUES (7, 5, '2019-09-25 07:25:54', '2019-09-25 07:25:54', NULL);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `menu_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL,
  `menu_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_show` tinyint(1) NOT NULL DEFAULT 1,
  `sort` int(11) NOT NULL DEFAULT 0,
  `lang` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, 1, 0, 'Trang Chủ', '/', 'home', '', 1, 1, 'vi', NULL, '2019-09-25 03:46:51', '2019-09-25 04:09:27');
INSERT INTO `menus` VALUES (2, 2, 0, 'Giới Thiệu', 'gioi-thieu', 'about', '', 1, 2, 'vi', NULL, '2019-09-25 04:26:43', '2019-09-25 07:11:10');
INSERT INTO `menus` VALUES (3, 3, 4, 'Vi Sinh', 'vi-sinh', 'type', '', 1, 1, 'vi', NULL, '2019-09-25 07:08:34', '2019-09-25 07:26:03');
INSERT INTO `menus` VALUES (4, 4, 0, 'Sản Phẩm', 'san-pham', 'null', '', 1, 3, 'vi', NULL, '2019-09-25 07:16:58', '2019-09-25 07:22:20');
INSERT INTO `menus` VALUES (5, 5, 4, 'Dinh Dưỡng', 'dinh-duong', 'type', NULL, 1, 2, 'vi', NULL, '2019-09-25 07:24:33', '2019-09-25 07:24:33');
INSERT INTO `menus` VALUES (6, 6, 4, 'Xử Lý Môi Trường', 'xu-ly-moi-truong', 'type', NULL, 1, 3, 'vi', NULL, '2019-09-25 07:25:23', '2019-09-25 07:25:23');
INSERT INTO `menus` VALUES (7, 7, 4, 'Diệt Khuẩn', 'diet-khuan', 'type', NULL, 1, 4, 'vi', NULL, '2019-09-25 07:25:53', '2019-09-25 07:25:53');
INSERT INTO `menus` VALUES (8, 8, 0, 'Thông Tin Kỹ Thuật', 'thong-tin-ky-thuat', 'category', '', 1, 4, 'vi', NULL, '2019-09-25 07:27:16', '2019-09-25 07:27:30');
INSERT INTO `menus` VALUES (9, 9, 0, 'Tin Tức', 'tin-tuc', 'category', NULL, 1, 5, 'vi', NULL, '2019-09-25 07:28:08', '2019-09-25 07:28:08');
INSERT INTO `menus` VALUES (10, 10, 0, 'Tuyển Dụng', 'tuyen-dung', 'category', '', 1, 6, 'vi', NULL, '2019-09-25 07:29:07', '2019-09-25 07:29:16');
INSERT INTO `menus` VALUES (11, 11, 0, 'Liên Hệ', 'lien-he', 'contact', NULL, 1, 7, 'vi', NULL, '2019-09-25 07:29:39', '2019-09-25 07:29:39');
INSERT INTO `menus` VALUES (12, 12, 0, 'Giỏ Hàng', 'gio-hang', 'cart', '', 0, 0, 'vi', NULL, '2019-10-04 07:42:27', '2019-10-04 07:46:15');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_06_07_092547_create_types_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_06_07_092557_create_products_table', 1);
INSERT INTO `migrations` VALUES (5, '2019_06_07_092743_create_product_types_table', 1);
INSERT INTO `migrations` VALUES (6, '2019_07_15_171057_create_languages_table', 1);
INSERT INTO `migrations` VALUES (7, '2019_07_15_172233_create_menus_table', 1);
INSERT INTO `migrations` VALUES (8, '2019_07_15_173740_create_contents_table', 1);
INSERT INTO `migrations` VALUES (9, '2019_07_16_101957_create_categories_table', 1);
INSERT INTO `migrations` VALUES (10, '2019_07_16_102230_create_menu_categories_table', 1);
INSERT INTO `migrations` VALUES (11, '2019_07_16_102328_create_menu_contents_table', 1);
INSERT INTO `migrations` VALUES (12, '2019_07_16_102429_create_content_categories_table', 1);
INSERT INTO `migrations` VALUES (13, '2019_07_16_151845_allow_null_target_menus_table', 1);
INSERT INTO `migrations` VALUES (14, '2019_07_19_161724_create_menu_products_table', 1);
INSERT INTO `migrations` VALUES (15, '2019_07_19_161841_create_menu_types_table', 1);
INSERT INTO `migrations` VALUES (16, '2019_08_14_104127_create_sponsors_table', 2);
INSERT INTO `migrations` VALUES (17, '2019_08_21_143215_add_url_target_sort_is_show_to_sponsors_table', 3);
INSERT INTO `migrations` VALUES (19, '2019_09_25_085431_create_carousels_table', 4);
INSERT INTO `migrations` VALUES (21, '2019_09_26_071649_create_contacts_table', 5);
INSERT INTO `migrations` VALUES (23, '2019_09_26_082049_add_video_to_contents_table', 6);
INSERT INTO `migrations` VALUES (27, '2019_09_26_092326_add_is_recruit_to_categories_table', 7);
INSERT INTO `migrations` VALUES (28, '2019_07_23_103255_add_is_featured_to_products_table', 8);
INSERT INTO `migrations` VALUES (30, '2019_10_04_094036_create_sessions_table', 9);
INSERT INTO `migrations` VALUES (35, '2019_10_07_000000_add_alias_to_products_table', 10);
INSERT INTO `migrations` VALUES (36, '2019_10_07_033910_add_description_to_products_table', 10);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for product_types
-- ----------------------------
DROP TABLE IF EXISTS `product_types`;
CREATE TABLE `product_types`  (
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`product_id`, `type_id`) USING BTREE,
  INDEX `product_types_type_id_foreign`(`type_id`) USING BTREE,
  CONSTRAINT `product_types_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `product_types_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of product_types
-- ----------------------------
INSERT INTO `product_types` VALUES (1, 1, NULL, NULL);
INSERT INTO `product_types` VALUES (2, 1, NULL, NULL);
INSERT INTO `product_types` VALUES (3, 1, NULL, NULL);
INSERT INTO `product_types` VALUES (4, 2, NULL, NULL);
INSERT INTO `product_types` VALUES (5, 2, NULL, NULL);
INSERT INTO `product_types` VALUES (6, 3, NULL, NULL);
INSERT INTO `product_types` VALUES (7, 3, NULL, NULL);
INSERT INTO `product_types` VALUES (8, 3, NULL, NULL);
INSERT INTO `product_types` VALUES (9, 3, NULL, NULL);
INSERT INTO `product_types` VALUES (10, 3, NULL, NULL);
INSERT INTO `product_types` VALUES (11, 3, NULL, NULL);
INSERT INTO `product_types` VALUES (12, 3, NULL, NULL);
INSERT INTO `product_types` VALUES (13, 3, NULL, NULL);
INSERT INTO `product_types` VALUES (14, 1, NULL, NULL);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ảnh đại diện',
  `product_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Loại sản phẩm',
  `alias` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT 'Alias',
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Mô tả',
  `receipt_price` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Giá nhập',
  `bill_price` decimal(10, 0) NOT NULL DEFAULT 0 COMMENT 'Giá bán',
  `stock` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Tồn kho',
  `is_show` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Hiển thị',
  `is_featured` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Sản phẩm nổi bật',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, 'upload/products/1.jpeg', 'Vi Sinh Deocare A Của Bayer gói 100g', 'vi-sinh-deocare-a-cua-bayer-goi-100g', 'Một sản phẩm của Công ty BAYER - công ty hàng đầu ngành thủy sản ở việt nam sản xuất theo tiêu chuẩn châu Âu', 100000, 150000, 0, 1, 0, 'Một sản phẩm của Công ty BAYER - công ty hàng đầu ngành thủy sản ở việt nam sản xuất theo tiêu chuẩn châu Âu', '2019-09-26 01:49:09', '2019-10-07 03:48:49', NULL);
INSERT INTO `products` VALUES (2, 'upload/products/2.jpeg', 'Chế phẩm sinh học hữu cơ vi sinh Tricho 500g', 'che-pham-sinh-hoc-huu-co-vi-sinh-tricho-500g', 'Phòng ngừ hiệu quả bệnh vàng lá, thối rễ, lở cổ rễ, nứt thân, xì mủ, chạy dây, chết nhanh, nấm hồng… do nấm bệnh như Furasium spp., Rhizoctonia solani, Phytophthora spp., Pythium spp.,Corticium spp.,… và tuyến trùng hại rễ gây ra.\r\nCung cấp dưỡng chất dễ tiêu và hệ vi sinh vật có ích giúp cải tạo đất, phục hồi, bảo vệ bộ rễ và kích thích rễ cây phát triển cực mạnh.\r\nTăng khả năng sinh trưởng, phát triển cây trồng. Tăng năng suất và chất lượng nông sản.\r\nPhân giải nhanh lân khó tiêu, cellulose, chitin, lignin, pectin,… thành chất dễ tiêu giúp cây trồng hấp thu tốt.\r\nDùng ủ phân, xử lý rác hữu cơ, làm đệm sinh học nuôi gia cầm gia súc', 200000, 300000, 0, 1, 0, NULL, '2019-09-26 03:24:06', '2019-10-07 03:50:37', NULL);
INSERT INTO `products` VALUES (3, 'upload/products/3.jpeg', 'Chế Phẩm Vi Sinh Em Gốc - 2 CHAI 1L', 'che-pham-vi-sinh-em-goc-2-chai-1l', NULL, 280000, 410000, 0, 1, 0, NULL, '2019-09-26 03:25:26', '2019-10-07 03:49:37', NULL);
INSERT INTO `products` VALUES (4, 'upload/products/4.jpeg', 'Thức ăn dán cho cá cảnh trọng lượng 150ml Sunsun', 'thuc-an-dan-cho-ca-canh-trong-luong-150ml-sunsun', NULL, 50000, 150000, 0, 1, 0, NULL, '2019-09-26 03:27:14', '2019-10-07 03:49:44', NULL);
INSERT INTO `products` VALUES (5, 'upload/products/5.png', 'Dinh Dưỡng Thủy Canh HyDroumart V (A + B) 1L', 'dinh-duong-thuy-canh-hydroumart-v-a-b-1l', NULL, 10000, 75000, 0, 1, 0, NULL, '2019-09-26 03:31:27', '2019-10-07 04:12:39', NULL);
INSERT INTO `products` VALUES (6, 'upload/products/6.gif', 'YUCCA ZEOLITE', 'yucca-zeolite', NULL, 20000, 45000, 0, 1, 0, NULL, '2019-09-26 03:34:20', '2019-10-07 04:12:32', NULL);
INSERT INTO `products` VALUES (7, 'upload/products/7.gif', 'IODINE 99', 'iodine-99', NULL, 69000, 69000, 0, 1, 0, NULL, '2019-09-26 03:35:10', '2019-10-07 04:12:25', NULL);
INSERT INTO `products` VALUES (8, 'upload/products/8.gif', 'NH - RAMOS', 'nh-ramos', NULL, 99999, 100000, 0, 1, 0, 'Công dụng\r\n- Diệt các loại vi khuẩn, nguyên sinh động vật, nấm trong nước ao nuôi.\r\n- Sát trùng dụng cụ nuôi, bể ương.\r\nSử dụng\r\n- Trước khi thả nuôi: 1kg/2000 m3 nước.\r\n- Xử lý ngừa bệnh: 1kg', '2019-09-26 03:36:56', '2019-10-07 04:12:18', NULL);
INSERT INTO `products` VALUES (9, 'upload/products/9.gif', 'NH - PRO YUCCA', 'nh-pro-yucca', NULL, 70000, 86000, 0, 1, 0, 'Công dụng\r\n- Hấp thu khí độc NH3, cải thiện môi trường nước.\r\n- Cấp cứu cá nổi đầu do bị khí độc.\r\nSử dụng\r\n- Đ?nh kỳ 5-7 ngày xử lý một lần: 500ml/8.000-10.000m3 nước ao nuôi.\r\n- Cấp cứu cá b', '2019-09-26 03:38:02', '2019-10-07 04:12:11', NULL);
INSERT INTO `products` VALUES (10, 'upload/products/10.png', 'Men vi sinh Hiếu khí Jumbo A', 'men-vi-sinh-hieu-khi-jumbo-a', NULL, 99999, 111000, 0, 1, 0, 'tập hợp nhiều vi sinh vật bacillus subtilis, lactobacillus acidophilus, Aspergillus oryzae. Men vi sinh Jumbo A được bổ sung vào bể hiếu khí với mục đích chính là bổ sung vi sinh vật cho lớp', '2019-09-26 03:55:19', '2019-10-07 04:12:03', NULL);
INSERT INTO `products` VALUES (11, 'upload/products/11.png', 'TIGER MEN', 'tiger-men', NULL, 123000, 234000, 0, 1, 0, NULL, '2019-09-26 03:57:30', '2019-10-07 04:11:56', NULL);
INSERT INTO `products` VALUES (12, 'upload/products/12.jpeg', 'Vi Khuẩn Quang Hợp PSB', 'vi-khuan-quang-hop-psb', NULL, 60000, 63000, 0, 1, 0, NULL, '2019-09-26 03:59:04', '2019-10-07 04:11:49', NULL);
INSERT INTO `products` VALUES (13, 'upload/products/13.jpeg', 'Vi sinh xử lý nước thải sinh hoạt BIO M1 chai 4 lít', 'vi-sinh-xu-ly-nuoc-thai-sinh-hoat-bio-m1-chai-4-lit', NULL, 310000, 420000, 0, 1, 0, NULL, '2019-09-26 03:59:58', '2019-10-07 04:11:42', NULL);
INSERT INTO `products` VALUES (14, 'upload/products/14.jpeg', 'EM - SHUIDG01', 'em-shuidg01', NULL, 29900, 40000, 0, 1, 0, NULL, '2019-09-28 12:07:15', '2019-10-07 04:11:35', NULL);

-- ----------------------------
-- Table structure for sessions
-- ----------------------------
DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions`  (
  `id` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `ip_address` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `user_agent` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `payload` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE INDEX `sessions_id_unique`(`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sponsors
-- ----------------------------
DROP TABLE IF EXISTS `sponsors`;
CREATE TABLE `sponsors`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `is_show` tinyint(1) NULL DEFAULT 1,
  `sort` int(10) UNSIGNED NULL DEFAULT 0,
  `target` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `sponsor_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `note` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of sponsors
-- ----------------------------
INSERT INTO `sponsors` VALUES (2, 'upload/sponsors/2.png', 1, 0, NULL, 'bien-dong', 'Bien Dong', NULL, '2019-09-25 08:36:42', '2019-09-25 08:42:05');
INSERT INTO `sponsors` VALUES (3, 'upload/sponsors/3.jpeg', 1, 0, NULL, 'ngoc-xuan', 'Ngoc Xuan', NULL, '2019-09-25 08:41:54', '2019-09-25 08:41:54');
INSERT INTO `sponsors` VALUES (4, 'upload/sponsors/4.jpeg', 1, 0, NULL, 'caseamax', 'caseamax', NULL, '2019-09-25 08:42:28', '2019-09-25 08:42:36');
INSERT INTO `sponsors` VALUES (5, 'upload/sponsors/5.jpeg', 1, 0, NULL, 'IDI', 'IDI', NULL, '2019-09-25 08:43:27', '2019-09-25 08:43:27');

-- ----------------------------
-- Table structure for types
-- ----------------------------
DROP TABLE IF EXISTS `types`;
CREATE TABLE `types`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `avatar` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ảnh đại diện',
  `type_name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Loại sản phẩm',
  `is_show` tinyint(1) NOT NULL DEFAULT 1 COMMENT 'Hiển thị',
  `sort` int(11) NOT NULL DEFAULT 0 COMMENT 'Sắp xếp',
  `note` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL COMMENT 'Ghi chú',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of types
-- ----------------------------
INSERT INTO `types` VALUES (1, NULL, 'Vi Sinh', 1, 1, NULL, '2019-09-25 07:15:36', '2019-09-25 07:22:55', NULL);
INSERT INTO `types` VALUES (2, NULL, 'Dinh Dưỡng', 1, 2, NULL, '2019-09-25 07:23:08', '2019-09-25 07:23:08', NULL);
INSERT INTO `types` VALUES (3, NULL, 'Xử Lý Môi Trường', 1, 3, NULL, '2019-09-25 07:23:27', '2019-09-25 07:23:27', NULL);
INSERT INTO `types` VALUES (4, NULL, 'Diệt Khuẩn', 1, 4, NULL, '2019-09-25 07:23:38', '2019-09-25 07:23:44', '2019-09-25 07:23:44');
INSERT INTO `types` VALUES (5, NULL, 'Diệt Khuẩn', 1, 4, NULL, '2019-09-25 07:23:38', '2019-09-25 07:23:38', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_username_unique`(`username`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'InnoSoft', 'innosoft', 'innosoftcantho@gmail.com', NULL, '$2y$10$sxYBstQmG42fM10.c6uV1uVnoH91wwMC1tGYyI0x6xeYIIGxpspNa', 1, '28UeSv44RPW7kjfIcVfDTHUZLrJHpAjfLdPW0EQ6N739bMlIayxjMGaB4Ttk', '2019-09-25 03:45:55', '2019-10-07 02:19:13', NULL);

SET FOREIGN_KEY_CHECKS = 1;
