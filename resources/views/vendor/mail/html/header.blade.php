<tr>
    <td class="header" style="background-color:#1150A0;">
        <a href="{{ $url }}" style="color:white;">
            {{ $slot }}
        </a>
    </td>
</tr>
