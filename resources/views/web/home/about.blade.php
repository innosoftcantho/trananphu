<div class="home-about bg-light py-3 py-lg-5" id="homeAbout">
    <div class="container">
        <h2 class="text-center text-uppercase font-weight-bold mb-2 mb-lg-4">
            {{ $about->title }}
        </h2>
        <div class="row d-flex align-items-center">
            <!-- <div class="col-lg-6 order-1 order-lg-0 mb-4 mb-lg-0">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe src="https://www.youtube.com/embed/{{ $about->embed }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-lg-6 order-0 order-lg-1 text-center text-lg-left"> -->
                <!-- <div class="fs-25 font-weight-medium text-uppercase mb-4">{{ $about->title }}</div> -->
                <div class="mb-4 mb-lg-0">
                    {{ $about->summary }}
                </div>
            <!-- </div> -->
        </div>
    </div>
</div>