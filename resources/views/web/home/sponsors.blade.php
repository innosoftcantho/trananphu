<div class="sponsors py-3 py-lg-5">
    <div class="container">
        <h2 class="text-center text-uppercase font-weight-bold mb-5">
            đối tác
        </h2>
        @if(count($sponsors) > 0)
        <div class="carousel slide modal-flick" data-type="multi" data-interval="3000" data-flickity='{
            "cellAlign": "center",
            "pageDots": false,
            "wrapAround": {{ (count($sponsors) > 5) ? "true" : "false" }},
            "cellAlign": "{{ (count($sponsors) > 5) ? "left" : "center" }}",
            "contain": true,
            "initialIndex": "{{ (count($sponsors) > 5) ? "0" : "1" }}",
            "prevNextButtons": {{ (count($sponsors) > 5) ? "true" : "false" }},
            "draggable": ">2",
            {{-- "autoPlay": true, --}}
            "freeScroll": false,
            "friction": 0.8,
            "selectedAttraction": 0.2} '>
            @foreach ($sponsors as $sponsor)
            {{-- col-12 col-sm-4 col-lg-2  --}}
            <div class="img-slide bg-white d-flex align-items-center justify-content-center mx-2 py-3 px-4">
                <a href="//{{ $sponsor->url }}" target="{{ $sponsor->target }}">
                    <img src="{{ asset($sponsor->avatar)}}" class="w-100">
                </a>
            </div>
            @endforeach
        </div>
        @endif
        {{-- <div class="carousel slide modal-flick"data-type="multi" data-interval="3000" data-flickity='{
            "cellAlign": "center",
            "pageDots": false,
            "wrapAround": "true",
            "cellAlign": "center",
            "contain": true,
            "initialIndex": 1,
            "prevNextButtons": false,
            "draggable": ">2",
            "autoPlay": true,
            "freeScroll": false,
            "friction": 0.8,
            "selectedAttraction": 0.2} '>
            @for ($i = 1; $i < 7; $i++)
                <div class="img-slide bg-white d-flex align-items-center justify-content-center mx-2 py-3 px-4">
                    <a href="#" target="#">
                        <img src="{{ asset('img/sponsors/'.(($i%5)+1).'.png')}}" class="w-100">
                    </a>
                </div>
            @endfor
        </div> --}}
    </div>
</div>