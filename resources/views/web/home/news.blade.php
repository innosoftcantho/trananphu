<div class="home-news bg-light py-3 py-lg-5">
    <div class="container">
        <h2 class="text-center text-uppercase font-weight-bold my-3">
            tin tức
        </h2>
        <div class="row justify-content-center">
            @forelse ($contents ?? [] as $content)
            <div class="col-12 col-md-8 col-xl-4 mb-4">
                <a href="{{ url("$menu_category->alias/$content->alias") }}">
                    <div class="card rounded-0 h-100 w-100">
                        <img class="w-100" src="{{ asset($content->avatar) }}">
                        <div class="card-body text-body mt-2">
                            <h5 class="card-title text-justify font-weight-bold mb-0">
                                {{ $content->title }}
                            </h5>
                            <div class="time my-3">
                                {{ $content->created_at->diffForHumans() }}
                            </div>
                            <div class="card-text text-justify">
                                {{ $content->summary }}
                            </div>
                            <div class="d-flex mt-5">
                                <div class="font-weight-medium fs-14 ml-auto">
                                    Xem tiếp <i class="fas fa-long-arrow-alt-right ml-2"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>    
            </div>
            @empty
            @endforelse
        </div>
    </div>
</div>