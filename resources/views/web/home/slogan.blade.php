@if (isset($banner))
    <div class="container mt-5">
        <a href="#">
            <img src="{{ asset($banner ? $banner->avatar : "") }}" class="w-100">
        </a>
    </div>
@else
@endif


{{-- <div class="slogan bg-center bg-contain bg-repeat" style="background-image: url( {{ asset($banner ? $banner->avatar : "") }} )">
    <div class="py-4 py-lg-5">
        <div class="row py-0 py-lg-5 mx-0">
            <div class="col-7 mx-auto px-0">
            </div>
        </div>
    </div>
</div> --}}