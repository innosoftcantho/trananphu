<div class="succeed my-5">
    <div class="container">
        <div class="row bg-cyan justify-content-center">
            <div class="col-12">
                <div class="block-blue bg-primary text-white">
                    <h2 class="text-uppercase font-weight-bold text-center pt-4">
                        Thành tựu
                    </h2>
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <div class="font-weight-medium text-center py-4">
                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem 
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-lg-around text-uppercase pt-4 py-lg-4">
                        <div class="col-lg-3 text-center pb-4 pb-lg-0">
                            <h1 class="font-weight-bold mb-2 mb-lg-4">50+</h1>
                            <h5 class="font-weight-bold">Năm kinh nghiệm</h5>
                        </div>
                        <div class="col-lg-3 text-center pb-4 pb-lg-0">
                            <h1 class="font-weight-bold mb-2 mb-lg-4">50+</h1>
                            <h5 class="font-weight-bold">khách hàng</h5>
                        </div>
                        <div class="col-lg-3 text-center pb-4 pb-lg-0">
                            <h1 class="font-weight-bold mb-2 mb-lg-4">50+</h1>
                            <h5 class="font-weight-bold">giải thưởng</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>