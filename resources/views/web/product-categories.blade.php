@extends('layouts.web')
@section('content')
<div class="product-categories">
    <div class="container">
        <div class="bg-center bg-cover" style="background-image: url( {{ asset('img/news.png') }} )">
            <div class="bg-overlay text-center header">
                <h2 class="centered text-uppercase text-white font-weight-bold">{{ $type->type_name ?? "" }}</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 my-5">
                <div class="row">
                    @foreach ($products as $product)
                        <div class="col-6 col-md-6 col-xl-4 mb-3">
                            @include('web.card.product')
                        </div>
                    @endforeach
                </div>
                <div class="row">
                    {{ $products->links('web.paginate') }}
                </div>
            </div>
            <div class="col-lg-3 mb-5 mt-lg-5">
                <h5 class="text-uppercase font-weight-bold">
                    danh mục sản phẩm
                </h5>
                @forelse ($types ?? [] as $type)
                <div class="d-flex border-top py-2">
                    <a href="{{ $type->menu() ? url( $type->menu()->alias) : "#" }}" class="font-weight-medium">
                        {{ $type->type_name }} 
                    </a>
                    <span class="ml-auto">{{ $type->products()->count() }}</span>
                </div>
                @empty
                @endforelse

                {{-- {{ dd(count($featured_products) < 1) }} --}}
                @if (count($featured_products) == 0)
                @else
                <div>
                    <h5 class="text-uppercase font-weight-bold my-4">
                        sản phẩm nổi bật
                    </h5>
                    @forelse ($featured_products ?? [] as $featured)
                        {{-- <a href="{{ $type->menu() ? url($type->menuType->menu->alias, [$product->alias]) : '#'}}"> --}}
                        <a href="{{ $product->type() ? url($product->type()->menuType->menu->alias, [$featured->alias]) : '#'}}">
                            <div class="card border-0 mb-4">
                                <div class="row">
                                    <div class="col-5 pr-0">
                                        <div class="image d-flex align-items-center justify-content-center">
                                            <img href="#" src="{{ asset($featured->avatar) }}" alt="Card image cap" 
                                            class="card-img-top">
                                        </div>
                                    </div>
                                    <div class="col-7">
                                        <div class="p-2">
                                            <div class="text-justify font-weight-bold">
                                                {{ $featured->product_name }}
                                            </div> 
                                        </div>
                                        <div class="font-weight-medium text-body px-2 pb-2">
                                            {{ number_format($product->bill_price) }} đ
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </a>
                    @empty
                    @endforelse
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

@push('js')
<script>
    // // Add to Cart

    // function sum(obj) {
    //     return Object.keys(obj).reduce((sum,key)=>sum+parseFloat(obj[key]||0),0);
    // }

    // $(".btn-cart").on("click", function () {
    //     var id = $(this).attr("data-id")
    //     axios.post('{{ route('add-product') }}', {
    //         product_id: id,
    //     })
    //     .then(function (response) {
    //         console.log(response);
    //         var products = response.data.products.cart;
    //         if(sum(products) > 9)
    //             $(".notify-badge").text("9+");
    //         else if (sum(products) == 0)
    //             $(".notify-badge").addClass("d-none");
    //         else
    //             $(".notify-badge").removeClass("d-none").text(sum(products));
    //     })
    //     .catch(function (error) {
    //         console.log(error);
    //     });
    // })
</script>
@endpush