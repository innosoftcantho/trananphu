@extends('layouts.web')
@section('content')

<div class="recruitment">
    <div class="bg-center bg-cover" style="background-image: url( {{ asset('img/job.png') }} )">
        <div class="bg-overlay text-center header">
            <h2 class="centered text-uppercase text-white font-weight-bold">tin tuyển dụng</h2>
        </div>
    </div>
    <div class="container">
        <div class="row mt-3">
            <div class="col-12 mt-4">
                @forelse ($contents ?? [] as $content)
                    <div class="card border-0 mt-4 pb-3">
                        <a href="/content">
                            <div class="row d-flex justify-content-center">
                                <div class="col-5 col-lg-4 pr-0 pr-lg-2">
                                    <div class="image d-flex align-items-center justify-content-center">
                                        <img src="{{ asset($content->avatar) }}" class="w-100">
                                    </div>
                                </div>
                                <div class="col-7 col-lg-6">
                                    <div class="card-body py-0 pl-0">
                                        <h4 class="card-title text-body font-weight-bold">
                                            {{ $content->title }}
                                        </h4>
                                        <div class="card-text text-muted">
                                            <div class="d-flex flex-wrap text-muted my-3">
                                                <div>Ngày đăng: {{ date('d-m-Y', strtotime($content->created_at)) }}</div>
                                            </div>
                                            <div class="text-body d-none d-lg-block mt-2">
                                                {{ $content->summary }}
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @empty
                    
                @endforelse
                {{-- @for($i=1; $i<10; $i++) --}}
                    {{-- <a href="#">
                        <div class="media mb-4 pb-3">
                            <img src="{{ asset('img/test/'.(($i%5)+1).'.jpg')}}" class="align-self-start">
                            <div class="media-body px-4">
                                <h4 class="font-weight-bold text-body mb-0">
                                    Tuyển dụng nhân viên kinh doanh {{$i}}-2019
                                </h4>
                                <div class="text-secondary my-3">
                                    <div>Ngày đăng: 1-10-2019</div>
                                </div>
                                <div class="d-none d-sm-block">
                                    <h4 class="text-body font-weight-normal">
                                        Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur                                                
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </a> --}}

                    {{-- <div class="card border-0 mt-4 pb-3">
                        <a href="#">
                            <div class="row d-flex justify-content-center">
                                <div class="col-5 col-lg-4 pr-0 pr-lg-2">
                                    <div class="image d-flex align-items-center justify-content-center">
                                        <img src="{{ asset('img/test/'.(($i%5)+1).'.jpg')}}" class="w-100">
                                    </div>
                                </div>
                                <div class="col-7 col-lg-6">
                                    <div class="card-body py-0 pl-0">
                                        <h4 class="card-title text-body font-weight-bold">
                                            Tuyển dụng nhân viên kinh doanh {{$i}}-2019
                                        </h4>
                                        <div class="card-text text-muted">
                                            <div class="d-flex flex-wrap text-muted my-3">
                                                <div>Ngày đăng: 1-10-2019</div>
                                            </div>
                                            <div class="text-body d-none d-lg-block mt-2">
                                                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur                                                  
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div> --}}
                {{-- @endfor --}}
            </div>
        </div>
        <div class="row my-4 pb-2">
            {{ $contents->links('web.paginate') }}
            {{-- <nav class="mx-auto" aria-label="Page navigation example">
                <ul class="pagination">
                    <li class="page-item arrow">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true"><i class="fas fa-angle-left"></i></span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item active"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item">
                        <a class="page-link arrow" href="#" aria-label="Next">
                            <span aria-hidden="true"><i class="fas fa-angle-right"></i></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav> --}}
        </div>
    </div>
</div>

@endsection