@extends('layouts.web')
@section('content')

<div class="contact">
    <div class="container">
        <div class="bg-center bg-cover" style="background-image: url( {{ asset('img/contact.png') }} )">
            <div class="bg-overlay text-center header">
                <h2 class="centered text-uppercase text-white font-weight-bold">liên hệ</h2>
            </div>
        </div>
    </div>
    <div class="container my-3 my-md-4" id="contact">
        <div class="p-3 p-md-4">
            <div class="row">
                <div class="col-md-7">
                    <form action="{{ route('web.contacts.store') }}" method="post">
                        @csrf
                        <h4 class="text-uppercase text-center text-md-left font-weight-bold mb-4">gửi tin nhắn cho chúng tôi</h4>
                        <div class="row">
                            <div class="col-12 col-lg-6 mb-3">
                                <input type="text" name="customer_name" class="form-control rounded-0" type="text" placeholder="Họ tên">
                            </div>
                            <div class="col-12 col-lg-6 mb-3">
                                <input type="text" name="email" class="form-control rounded-0" type="text" placeholder="Email">
                            </div>
                        </div>
                        <textarea type="text" name="content"  class="form-control rounded-0 my-md-2" id="exampleFormControlTextarea1" placeholder="Nội dung" rows="8" style="resize: none;"></textarea>
                        <div class="row mb-4">
                            <div class="col-lg-5 ml-auto">
                                <button type="submit" class="btn btn-primary btn-send btn-block text-uppercase rounded-0 mt-3">Gửi</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-4 ml-md-4">
                    <h4 class="text-uppercase font-weight-bold mb-4">thông tin liên hệ</h4>
                    <div class="d-flex mb-3">
                        <h5 class="mr-3 pr-2"><i class="fas fa-envelope"></i></h5>
                        {{-- {{ dd($data['email']) }} --}}
                        <span>Email: {{ $data['email'] ?? '' }}</span>
                    </div>
                    <div class="d-flex mb-3">
                        <h5 class="mr-3 pr-2"><i class="fas fa-phone-alt"></i></h5>
                        <span>Điện thoại: {{ $data['phone'] ?? '' }}</span>
                    </div>
                    <div class="d-flex mb-3">
                        <h5 class="mr-3 pr-2"><i class="fas fa-map-marker-alt"></i></h5>
                        <span class="ml-1">Địa chỉ: {{ $data['address'] ?? '' }}</span>
                    </div>
                    <h4 class="text-uppercase font-weight-bold my-4 pt-xl-4">theo dõi chúng tôi</h4>
                    <div class="d-flex mt-4 my-md-4">
                        <a href="//{{ $data['facebook'] ?? '' }}">
                            <div class="icon rounded-circle bg-light text-body mr-4">
                                <i class="fab fa-facebook-f w-100"></i>
                            </div>
                        </a>
                        <a href="//{{ $data['youtube'] ?? '' }}">
                            <div class="icon rounded-circle bg-light text-body mr-4">
                                <i class="fab fa-youtube w-100"></i>
                            </div>
                        </a>
                        <a href="//{{ $data['instagram'] ?? '' }}">
                            <div class="icon rounded-circle bg-light text-body mr-4">
                                <i class="fab fa-instagram w-100"></i>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-center bg-cover map">
        <iframe id="gmap_canvas" width="100%" height="100%" src="https://maps.google.com/maps?q=can%20tho%20%20Hi%E1%BB%87p%20h%E1%BB%99i%20C%C3%A1%20tra%20VN%20&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
    </div>
</div>

@endsection

@push('ready')
    @if ($message = Session::get('info'))
    toastr["info"]("{{$message }}");
    @endif
    @if (count($errors) > 0)
    toastr["error"]("@foreach ($errors->all() as $error) <li>{{ $error }}</li> @endforeach");
    @endif
@endpush