{{-- <div class="overview bg-center bg-cover bg-no-repeat" style="background-image: url( {{ asset('img/img/background.png') }} )">
    <div class="bg-overlay h-100">
        <div class="container d-flex align-items-center justify-content-center h-100">
            <div class="overview-text text-center">
                <h2 class="text-uppercase text-white mb-4">
                    giới thiệu công ty
                </h2>
            </div>
        </div>
    </div>    
</div> --}}
<div class="overview pb-5">
    <div class="container-fluid px-0">
        <div class="row mx-0">
            <div class="col-12 px-0">
                <img src="{{ asset('img/background-4.png') }}" class="w-100">
                <div class="title">
                    <h2 class="text-uppercase text-white font-weight-bold">
                        giới thiệu công ty
                    </h2>
                </div>
            </div>
        </div>
    </div>
    <div class="about-text">
        <div class="container">
            <div class="row">
                <div class="video col-10 col-lg-6 my-4 my-xl-0 mx-auto">
                    <div>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe src="https://www.youtube.com/embed/{{ $about->embed }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="summary col-lg-10 mt-xl-3 mx-auto">
                    <div>
                        {{ $about->summary }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>