@extends('layouts.web')
@section('content')

<!-- Slideshow -->
@include('web.home.slideshow')

<!-- About -->
@include('web.home.about')

<!-- Products -->
@include('web.home.products')

<!-- Slogan -->
@include('web.home.slogan')

<!-- News -->
@include('web.home.news')

<!-- Succeed -->
{{-- @include('web.home.succeed') --}}

<!-- Sponsors -->
@include('web.home.sponsors')

@endsection

@push('js')
    <script>
        // if (screen.width >= 992) {
        //     new WOW().init();
        // }
    </script>
@endpush
